from rest_framework import serializers 
from rest_framework.serializers import Serializer

from mainapp.models import Blogs, Challenges, Comments_challenges, Comments_blogs, Ads, Biografia

class BlogsSerializer(serializers.ModelSerializer):
    class Meta: 
        model = Blogs
        fields =('id','titulli','permbajtja','data','foto','admin_id')

class ChallengesSerializer(serializers.ModelSerializer):
    class Meta: 
        model = Challenges
        fields =('id','titulli','text','foto','file','data','expire_date')

class Comments_challengesSerializer(serializers.ModelSerializer):
    user= serializers.ReadOnlyField(source="user_id.username")
    class Meta: 
        model = Comments_challenges
        fields =('id','user_id','Challenges_id','text','data','file','user')

class Comments_blogsSerializer(serializers.ModelSerializer):
    user= serializers.ReadOnlyField(source="user_id.username")
    class Meta: 

        model = Comments_blogs
        
        fields =('id','user_id','Blogs_id','text','user')

class AdsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ads
        fields = ('id', 'titulli', 'foto', 'link')

class BiografiaSerializer(serializers.ModelSerializer):
    class Meta:
        model=Biografia
        fields=("full_name","pozita","foto","description")

'''
class UserPasswordChangeSerializer(Serializer):
    old_password = serializers.CharField(required=True, max_length=30)
    password = serializers.CharField(required=True, max_length=30)
    confirmed_password = serializers.CharField(required=True, max_length=30)

    def validate(self, data):
        # add here additional check for password strength if needed
        if not self.context['request'].user.check_password(data.get('old_password')):
            raise serializers.ValidationError({'old_password': 'Wrong password.'})

        if data.get('confirmed_password') != data.get('password'):
            raise serializers.ValidationError({'password': 'Password must be confirmed correctly.'})

        return data

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.save()
        return instance

    def create(self, validated_data):
        pass

    @property
    def data(self):
        # just return success dictionary. you can change this to your need, but i dont think output should be user data after password change
        return {'Success': True}
'''
from rest_framework import serializers

class ChangePasswordSerializer(serializers.Serializer):

    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

