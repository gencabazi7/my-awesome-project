from rest_framework.generics import *
from django_filters.rest_framework import DjangoFilterBackend
import datetime

import json
from rest_framework import renderers
from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.http import HttpResponse, JsonResponse
from rest_framework import filters
from rest_framework import viewsets
from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions,status
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
#from django_filters import rest_framework as filterss 
from django.contrib.auth import authenticate
from rest_framework.response import Response
from rest_framework.status import HTTP_401_UNAUTHORIZED
from rest_framework.status import HTTP_409_CONFLICT
from rest_framework.status import HTTP_200_OK
from rest_framework.authtoken.models import Token
import django_filters
from django_filters import FilterSet
from rest_framework import generics
#############################################################

from rest_framework.generics import ListAPIView, RetrieveAPIView
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.contrib import auth, messages
from rest_framework.decorators import api_view, permission_classes,action

#Search 
from django.shortcuts import render
from django.db.models import Q

#Change password
from . import serializers
from django.views.decorators.csrf import csrf_exempt

from mainapp.models import Blogs, Challenges, Comments_challenges, Comments_blogs, RegisterForm, Ads, Biografia
from .serializers import BlogsSerializer, ChallengesSerializer, Comments_challengesSerializer, Comments_blogsSerializer, AdsSerializer, ChangePasswordSerializer, BiografiaSerializer


@api_view(["POST"])
@permission_classes((permissions.AllowAny,))
def Login(request):

    username = request.data.get("username")
    password = request.data.get("password")

    user = authenticate(username=username, password=password)
    if not user:
       return Response({"error": "Login failed"}, status=HTTP_401_UNAUTHORIZED)

    token, _ = Token.objects.get_or_create(user=user)
    return Response({"token": token.key},status=status.HTTP_200_OK)

@api_view(["POST"])
@permission_classes((permissions.AllowAny, ))
def register(request):
    username=request.data.get("username")
    password=request.data.get("password")
    email=request.data.get("email")
    firstName=request.data.get("first_name")
    lastName=request.data.get("last_name")
    if(request.user.is_authenticated):
        return Response({
            'alreadyAuthenticated': True
        }, status=HTTP_409_CONFLICT)
    users = User.objects.all()
    for i in users:
        if (email==i.email or username==i.username):
            return Response({'message':'a user with this email or username already exists'}, status=HTTP_409_CONFLICT)
    user = User.objects.create_user(username,email)
    user.first_name=firstName
    user.last_name=lastName
    user.set_password(password)
    user.save()
    login(request._request,user)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({"token": token.key},status=status.HTTP_200_OK)


@csrf_exempt
def register_blog_comment(request):
    try:
        _user=User.objects.get(**{User.USERNAME_FIELD:request.POST.get('username')})
        _blog=Blogs.objects.get(pk=int(request.POST.get('blog_id')))
        _comment=request.POST.get('comment')
        _date=datetime.datetime.now()

        new_comment=Comments_blogs(user_id=_user,Blogs_id=_blog,text=_comment,data=_date)
        new_comment.save()
        return JsonResponse({"succes": True},status=status.HTTP_200_OK)
    except:
        return JsonResponse({"succes": False},status=status.HTTP_200_OK)
    return JsonResponse({"succes": False},status=status.HTTP_200_OK)
    
def getBlogComments(request,blogId):
    try:
        blog=Blogs.objects.get(pk=int(blogId))
        comments=Comments_blogs.objects.filter(Blogs_id=blog)
        serializer=Comments_blogsSerializer(comments,many=True)
        return JsonResponse({"comments":serializer.data},status=status.HTTP_200_OK)
    except:
        return JsonResponse({"succes": False},status=status.HTTP_204_NO_CONTENT)

@csrf_exempt
def register_challenge_comment(request):
    try:
        _user=User.objects.get(**{User.USERNAME_FIELD:request.POST.get('username')})
        _challenge=Challenges.objects.get(pk=int(request.POST.get('challenge_id')))
        _comment=request.POST.get('comment')
        _date=datetime.datetime.now()

        new_comment=Comments_challenges(user_id=_user,Challenges_id=_challenge,text=_comment,data=_date)
        new_comment.save()
        return JsonResponse({"succes": True},status=status.HTTP_200_OK)
    except:
        return JsonResponse({"succes": False},status=status.HTTP_200_OK)
    return JsonResponse({"succes": False},status=status.HTTP_200_OK)
    
def getChallengeComments(request,challengeId):
    try:
        challenge=Challenges.objects.get(pk=int(challengeId))
        comments=Comments_challenges.objects.filter(Challenges_id=challenge)
        serializer=Comments_challengesSerializer(comments,many=True)
        return JsonResponse({"comments":serializer.data},status=status.HTTP_200_OK)
    except:
        return JsonResponse({"succes": False},status=status.HTTP_204_NO_CONTENT)


'''
@api_view(["POST"])
@permission_classes((permissions.AllowAny, ))


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('change_password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'accounts/change_password.html', {
        'form': form
    })
'''

class BlogsListView(generics.ListAPIView):
    queryset = Blogs.objects.all()
    serializer_class = BlogsSerializer
    filter_backends=(DjangoFilterBackend, filters.SearchFilter)
    search_fields = ('titulli',)

class BlogsDetailView(RetrieveAPIView):
    queryset = Blogs.objects.all()
    serializer_class = BlogsSerializer

class ChallengesListView(generics.ListAPIView):
    queryset = Challenges.objects.all()
    serializer_class = ChallengesSerializer
    filter_backends=(DjangoFilterBackend, filters.SearchFilter)
    search_fields = ('titulli',)

class ChallengesDetailView(RetrieveAPIView):
    queryset = Challenges.objects.all()
    serializer_class = ChallengesSerializer


class Comments_challengesListView(ListAPIView):
    queryset = Comments_challenges.objects.all()
    serializer_class = Comments_challengesSerializer

class Comments_challengesDetailView(RetrieveAPIView):
    queryset = Comments_challenges.objects.all()
    serializer_class = Comments_challengesSerializer


class Comments_blogsListView(ListAPIView):
    queryset = Comments_blogs.objects.all()
    serializer_class = Comments_blogsSerializer

class Comments_blogsDetailView(RetrieveAPIView):
    queryset = Comments_blogs.objects.all()
    serializer_class = Comments_blogsSerializer

'''
class APIChangePasswordView(UpdateAPIView):
    serializer_class = UserPasswordChangeSerializer
    model = User() # your user model
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        return self.request.user
'''

class AdsListView(ListAPIView):
    queryset = Ads.objects.all()
    serializer_class = AdsSerializer

class AdsDetailView(RetrieveAPIView):
    queryset = Ads.objects.all()
    serializer_class = AdsSerializer

class BiografiaListView(ListAPIView):
    queryset=Biografia.objects.all()
    serializer_class=BiografiaSerializer

class BiografiaDetailView(RetrieveAPIView):
    queryset=Biografia.objects.all()
    serializer_class=BiografiaSerializer

#Password_change
class ChangePasswordView(UpdateAPIView):
        """
        An endpoint for changing password.
        """
        serializer_class = ChangePasswordSerializer
        model = User
        permission_classes = (IsAuthenticated,)

        def get_object(self, queryset=None):
            obj = self.request.user
            return obj

        def update(self, request, *args, **kwargs):
            self.object = self.get_object()
            serializer = self.get_serializer(data=request.data)

            if serializer.is_valid():
                # Check old password
                if not self.object.check_password(serializer.data.get("old_password")):
                    return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
                # set_password also hashes the password that the user will get
                self.object.set_password(serializer.data.get("new_password"))
                self.object.save()
                return Response("Success.", status=status.HTTP_200_OK)

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


