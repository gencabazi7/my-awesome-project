# Generated by Django 2.1.5 on 2019-05-17 12:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0002_auto_20190514_1630'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ads',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('titulli', models.CharField(max_length=30)),
                ('foto', models.ImageField(upload_to='')),
                ('link', models.URLField()),
            ],
        ),
    ]
