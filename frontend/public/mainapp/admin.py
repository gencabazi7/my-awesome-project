from django.contrib import admin

from .models import Blogs, Challenges, Comments_challenges, Comments_blogs, Ads 

admin.site.register(Blogs)
admin.site.register(Challenges)
admin.site.register(Comments_challenges)
admin.site.register(Comments_blogs)
admin.site.register(Ads)