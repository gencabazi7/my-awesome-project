from django.urls import path, include
from . import views 
from django.views.decorators.csrf import csrf_exempt

from .views import  BlogsListView, BlogsDetailView, ChallengesListView, ChallengesDetailView, Comments_challengesListView, Comments_challengesDetailView, Comments_blogsListView, Comments_blogsDetailView, AdsListView, AdsDetailView, ChangePasswordView
from django.conf.urls.static import static
from django.conf import settings

from django.contrib.auth import views as auth_views

urlpatterns = [
    path('blogs/', csrf_exempt(BlogsListView.as_view())),
    path('blogs/<pk>', BlogsDetailView.as_view()),

    path('challenges/', ChallengesListView.as_view()),
    path('challenges/<pk>', ChallengesDetailView.as_view()),

    path('commentschallenges/', Comments_challengesListView.as_view()),
    path('commentschallenges/<pk>', Comments_challengesDetailView.as_view()),

    path('commentsblogs/', Comments_blogsListView.as_view()),
    path('commentsblogs/<pk>', Comments_blogsDetailView.as_view()),

    path('ads/', AdsListView.as_view()),
    path('ads/<pk>', AdsDetailView.as_view()),

    path('register/', views.register, name='register'),

    path('login/', views.Login, name='login'),
    path('registerCommentBlog/', views.register_blog_comment, name='commentBlog'),
    path('getBlogComments/<blogId>',views.getBlogComments,name='getBlogComments'),

    path('registerCommentChallenge/', views.register_challenge_comment, name='commentChallenge'),
    path('getChallengeComments/<challengeId>',views.getChallengeComments,name='getChallengeComments'),

    #Password_reset
    path('', include('django.contrib.auth.urls')),

    #path('changepassword/', views.APIChangePasswordView, name="change_password"),
    path('change-password/', ChangePasswordView.as_view()),
    
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

