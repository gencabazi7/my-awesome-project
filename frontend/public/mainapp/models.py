from django.db import models
from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import UserCreationForm

class Blogs (models.Model):
    id = models.AutoField(primary_key=True)
    titulli = models.CharField(max_length=100)
    permbajtja =models.TextField()
    data = models.DateTimeField(auto_now_add=True)
    foto = models.ImageField(upload_to='')
    admin_id = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.titulli

class Challenges (models.Model):
    id = models.AutoField(primary_key=True)
    titulli = models.CharField(max_length=100)
    text = models.TextField()
    foto = models.ImageField(upload_to='')
    file = models.FileField(upload_to='')
    data = models.DateTimeField(auto_now_add=True)
    expire_date = models.DateTimeField()

    def __str__(self): 
        return self.titulli

class Comments_challenges (models.Model):
    id = models.AutoField(primary_key = True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    Challenges_id = models.ForeignKey(Challenges, on_delete=models.CASCADE)
    text = models.TextField()
    data = models.DateTimeField(auto_now_add=True)
    file = models.FileField(upload_to='')

    def __str__(self):
        return self.text

class Comments_blogs (models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    Blogs_id = models.ForeignKey(Blogs, on_delete=models.CASCADE) 
    text = models.TextField()
    data = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.text

class Ads (models.Model):
    id = models.AutoField(primary_key=True)
    titulli = models.CharField(max_length=30)
    foto=models.ImageField()
    link=models.URLField()

    def __str__(self):
        return self.foto

class RegisterForm(UserCreationForm):
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')
