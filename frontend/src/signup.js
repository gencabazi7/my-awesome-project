import React from 'react';
import './HomePage/Styling/css/signup.css';
import MainHeader from './Header';
import userServices from './API/auth';
import AlertDialog from './Alerterror';

import Footer from './HomePage/Footer';

class Signup extends React.Component {
  constructor(props){
    super(props)
    this.state={
      signupdata:null,
      fullname:'',
      username:'',
      email:'',
      password:'',
      confirmPassword:'',
      submitted: false,
      error:""
    }
  }
 
  handleChange=(event)=>{
    const {name,value}= event.target;
    this.setState({[name]:value})
  }
  handleSubmit=(e)=> {
    e.preventDefault();

    this.setState({ submitted: true });
    const {fullname,username,email,password,confirmPassword, returnUrl } = this.state;
    const rules ={
      password:'required|string|min:6|confirmed'
    };
 
    

    // stop here if form is invalid
    if (!(username && password && fullname && email && confirmPassword)) {
        return;
    }

    this.setState({ loading: true });
    userServices.signup( fullname,username,email,password,confirmPassword)
        .then(
            user => {
                const { from } = this.props.location.state || { from: { pathname: "/" } };
                this.props.history.push(from);
            }
            
        )
        // const messages = {
        //   'password.confirmed': 'The password does not match'
        // }
        // validateAll(password, rules)
        .then(()=>{
        
        })
        .catch(error=> {
          if (error.response) {
            this.setState({error:<AlertDialog/>})
            
          }
        });;
}
    render() {
      let {fullname,username,email,password,confirmPassword}=this.state
      return (
  <div>
      <MainHeader/>
      <div className='signupbody'>
        <br />
        <div className="row">
          <div className="border col-md-6 offset-md-3 col-lg-6 offset-lg-3">
            <h3 style={{marginTop: '10px'}} align="center">Create a New Account</h3>
            <h3 style={{marginTop: '10px'}} align="center">{this.state.error}</h3>
            <form name="form" onSubmit={this.handleSubmit}>
            <div className="container text-left">
              <div className=' width:400px'>
                <label htmlFor="uname"><b>Full Name</b></label>
                <input style={{marginLeft: '75px'}} className='inputat' type="text" placeholder="Full Name" name="fullname" value={fullname} onChange={this.handleChange} required />
                <hr className='hrii' />
                <label htmlFor="uname"><b>Username</b></label>
                <input style={{marginLeft: '75px'}} className='inputat' type="text" placeholder="Username" name="username" value={username} onChange={this.handleChange} required />
                <hr className='hrii' />
                <label htmlFor="uname"><b>Email</b></label>
                <input style={{marginLeft: '110px'}} className='inputat' type="text" placeholder="Email" name="email" value={email} onChange={this.handleChange} required />
                <hr className='hrii'/>
              
                <label htmlFor="psw"><b>Password</b></label>
                <input style={{marginLeft: '75px'}} className='inputat' type="password" placeholder="Password" name="password" value={password} onChange={this.handleChange} required />
                <hr className='hrii'/>
                <label htmlFor="psw"><b>Confirm Password</b></label>
                <input style={{marginLeft: '10px'}} className='inputat' type="password" placeholder="Confirm Password" name="confirmPassword" value={confirmPassword} onChange={this.handleChange} required />
                <br />
                <button style={{textAlign: "center"}}className='butonat butonat-hover' type="submit">Sign Up </button>
              </div>
              <div className="container" style={{backgroundColor: '#eeeeee'}}>
              </div>
            </div>
            </form>
          </div>
        </div></div>
       
        <Footer/>
          </div>
  
      );
    }
  }
  export default Signup;