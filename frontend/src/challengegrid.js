import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import ImgMediaCard from './challenge';
import ImgMediaCardd from './ads';
import Foto1 from './HomePage/Styling/Images/0_ngXgBNNdx6iiWP8q.png';
import Foto2 from './HomePage/Styling/Images/download.png';
import Foto3 from './HomePage/Styling/Images/52eabf633ca6414e60a7677b0b917d92-male-avatar-maker.jpg';
import './HomePage/Styling/css/blogstyle.css';
import Footer from './HomePage/Footer';
import MainHeader from './Header';
import challengeServices from './API/challenges';


const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  // button: {
  //           backgroundColor:"green",
  //       },
});

class Challengegrid extends React.Component {
  
  constructor(props){
    super(props)
    this.state={Challenge:[],Comments:[],username:JSON.parse(localStorage.getItem('user')).authdata,TextComment:"",file:null}
  }
  onValueChange=(e)=>{
    const target= e.target;
    const value=target.value
    const name= target.name
    this.setState({
      [name]: value
  
    })
  
  
  }
  onSubmit=(e)=>{
    const { id } = this.props.match.params
    let {username,TextComment}=this.state
    e.preventDefault()
    let data= new FormData()
    data.append("username",username)
    data.append("challenge_id",id)
    data.append("comment",TextComment)
    data.append('file',this.file);
  
    challengeServices.registerChallengeComment(data).then(response=>{
      if (response.data.succes==true){
        challengeServices.getChallengeComments(id).then(response =>{
          this.setState({Comments:response.data.comments?
          response.data.comments:[],TextComment:""})
        })
  
      }
      else {
        
      }
  
      
    })
  
  
  
  }
  
  spinner=()=>{
    return(  <div style={{marginTop:170}} className="text-center">
      <div class="spinner-grow text-muted"></div>
      <div class="spinner-grow text-primary"></div>
      <div class="spinner-grow text-success"></div>
      <div class="spinner-grow text-info"></div>
      <div class="spinner-grow text-warning"></div>
      <div class="spinner-grow text-danger"></div>
      <div class="spinner-grow text-secondary"></div>
      <div class="spinner-grow text-dark"></div>
      <div class="spinner-grow text-light"></div>
    </div>)

  }

  componentDidMount(){
    const {id}=this.props.match.params
    challengeServices.getChallenge(id).then(response=>{
      this.setState({challenge:response.data})
      
    })
    challengeServices.getChallengeComments(id).then(response =>{
      this.setState({Comments:response.data.comments})
      
    })
  }
  handleUploadFile=(event)=>{
    this.file=event.target.files[0];
}
handleFileVersion=(event)=>{
    this.file=event.target.value;
}

  renderComments =(comments)=>{
    if(comments){
    return comments.map(item=>{
      return(
        <div className="row">
    <div className="col-sm-2 text-center">
      <img src={Foto3} className="img-circle" height={65} width={65} alt="Avatar" />
    </div>
    <div className="col-sm-10">
      <h5>{item.user} <small>{item.data}</small></h5>
      <p>{item.text}</p>
      <br />
    </div>
    </div>
    )
    })
  }
  }
render (){
  let challenge=this.state.Challenge
  let comments=this.state.Comments
  
  const { classes } = this.props;
  return (
    <div>
      <MainHeader colorrr='#ff3b05'/>
    <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item xs={2}>
          
        </Grid>
        <Grid item xs={8}>
        {challenge? <ImgMediaCard challenge={challenge}/>:this.spinner()}
        
        </Grid>
        <Grid item xs={2}>
        <ImgMediaCardd/>

        </Grid>
      
       
        <Grid item xs={1}>
        </Grid>
        
      </Grid>



      <h4>Leave a Comment:</h4>
          <form role="form" onSubmit={this.onSubmit}>
            <div className="form-group">
            <textarea value={this.state.TextComment} onChange={this.onValueChange} name="TextComment" className="form-control" rows={3} required defaultValue={""} />
            </div>
            <input type="submit" style={{height:44,width:100,marginRight:5}} className="btn btn-success" value="Submit"/>
            <input type="file" className="btn btn-success" name="file"  onChange={this.handleUploadFile}/>
          </form>
          <br /><br />          
            {comments!=[]?this.renderComments(comments):''}
          
    </div>
    <Footer/>
    </div>
  );
}
 
}

Challengegrid.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Challengegrid);
// form-control-file border