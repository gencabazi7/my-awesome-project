import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


const styles = theme => ({
  card: {
    minWidth: 300,
    maxWidth: 300,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
});

class RecipeReviewCard55 extends React.Component {
  state = { expanded: false };

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  render() {
    const { classes } = this.props;

    return (
      <Card className={classes.card}>
        <CardHeader
          
          title="Shukrie Fisteku-Web Developer"
          
        />
        <CardMedia
          className={classes.media}
          image="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEBAQEA8VFRAVFRUVFRcPFhUVFhUVFRUWFhYWFhUYHSggGBomGxUVIjEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGi0lHSUrLS0tLS0tKy0tLS0tLS0tLS0tKystLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMYA/wMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAACAwEFAAQGBwj/xAA4EAABAwIEBAQEBQMEAwAAAAABAAIRAyEEBRIxBkFRYSJxgZETMqGxQlLB0fAH4fEUI2KCFXKS/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAEDAgQF/8QAJREAAgICAgICAgMBAAAAAAAAAAECEQMSITEEUSJBEzNhcYEy/9oADAMBAAIRAxEAPwD1ZrUYCwBEExGQpAUqQgRgCmFICkBAGAKYWKUARClYpQBELFKguCQzFi08ZmVKkNT3ho6ut/ladHiLDP2rMPk4HnF+nr1QBcrEhmIBAMiPMX8k1rweaAChYslZKAIWQpWIAiFEIlCQAwoIRoSgYJQlGUJQAsoHBMKApAKcEshNcllAwCEtwTSluSY0WARBQEQVCZKkKAiCAJCkLApQBilYpQBCxYSqrP8AP6GCpOrV36WjbmSejQNygZv4jENY0ucQAN5Xm/Ef9UqQlmEplzgfnq2b6AXP0XB8Tca4nHVC41HU6Nw2mwwI/wCUXcT3MWXPeC17+/0U3MrHH7LfNM8xGMeH1amqDIgAQT07JVLVr1CqG7bTM94WlLT4SY7DfzspqHSPAbd5+sXUnJl1FItn18VAjEuJ2u9w8PRW+S8UZhQLR8QvaDs+4ju4LmaWMfAmD03S6mZvkgAAd5CFKQOMD3HIeOKNfw1C2nUG4c4aT5OK6ehi21Bqa4EdRcL5rw+Ln8MkcwIj1XT8N8QVaTgGPIE+Jkzr7tJ5xytK2sjXZN4U/wDk92BUrl+HeIqdZrZcZMDS+zwTAHmJMLpgNoVE0+iDTXDCWKViYgSoKJQUAAUJRlCUhgFLKYUBQAspZTCllIYBS3JhSnpMZZBGAhCMKpMkKQsCkIAkKVilIDFBKlC8wEAVHEGc08JRdUqPa2x0h7g3U6JgTuV8+8WcR1sbVPxHDS2dDW8pge55mV0n9Wcd8fGtY0uPwmEQNhqMk9raR6LgHQDJJ+/83UpStloxpWC6pHMgDpv/ADZFScG6iAdWwJ5eSl1wAR5JmEwlQnwtnyusNlYpsWyk8CQCXHeJTqdRw/AT5WPqOassNl1YxIMdIJ+gVrhMgrOFmzPMiIHqLKbyIssUjmsIWh0vc4NnmDA6eqsMRSouENfflqtPeyuKvDlZrg5zNXvEfqOye3gxtUSRp/8AW8eUkLDyxvs3+J0cph3fBcRUktHKNzynsrChV1iSYMkgjlEdOS3sbkj6bdBa5wbs5w/D+U39QfNVtHDPpO1MYC2LlpBFxzAPl0Wt0zOrR1GVZg5rmuc29tVjB8+hHXbyXr+SZi2tSa4GYsf794heHYHH65Bhuk/KbEdC0wu44QxZ1CmI1AcpaXjsRuUQyOMqMZcalGz05pUrRwdaQHAy07TuDzBW8CutOziaoxQVKgpiBKAoyhKQxZQFMKW5AC3JZTHICkMW5KemuSXpMZahGEIRBVJkhEFAUoAlSoWIAlV2cYvRSe4bgG20mDZbxVJxDjW0KdSq4SGMc76H9AfdJgj57z2tUqVnuc6XOMugEAG0yLXG3oq1h2i/Xp5+a2M3xpc97nbucSY7mY+q0sO/SYPeZ8lzro6/ss8JhtbgPsu6yfK2MAsFyuQkFwi67zAclyZpc0duKNKzbp4Bn5R7Kxw+GA5IKBlb9EWWIo1Kw6eGB3C3aOEZvpCTRW5SV40RlZp43KadQHwrzbiDJa2Hque2nDCCC53yxO5g2/uvXdC0s1wLa1NzDzCo4IkptHiAFOrqaHhtYCxAtYyelr39+yuske+maRaSSNiJiPTlcLm88y5+GxDxpLXB079Bbfkt/JszfrbSBBJu0QN3R4TbbcTvCllxNRtMpjyW6Z7dlFZtYOcBYlp8nHcW52VvRsYmxk33ERaee6ouG6LqNAAgaXHV4Zsdpk/hMekjfle0nBxJ2i1955rpx3qr7OLJWzrocoKJQVYkCUBRlAUDAKW5MKW5IBbkDkbkBSGLckvTnJL0MC2CIIQjCoYJClQESAMUrFiAIK5PjwkYPEQA4aCHTMRz26ALqyVoZlgBXp1KbjZw/nok+gXZ8t16cE3F+fT3WmHnVBuun42yv/R4qtRDfDMt3NndCReL+y5ihTl0Dn/CopHTdnb8L0QGSuvwYlc5k9EUqYnYCSrXAHFVr0aYDeTnkADv39FwTW0j0ofGKOuwFGQrFtGFz+ByzHsMurtjoNvSy3//ACJp+Gq4TyWuIoxez4LnC00eLxQpCzS53QIsnrB7ZC0OJ84bhqb6gbLuXLtJPIDqtXrGzFOU9SWVMVWI8DWN/wCRg/Yn7Kxp4Kq0fMHDoZn0JXk1LjHH1KzmNIsJjxNB2gNDPFJnnK7/AIUzrFvLmYnDOa0fLVa7Ux3o6Hj1CvC6tk8irhHJf1Qy5xqUXskF/gMCZM278/qg4Q4beKwLw3VTAc3U0kmeYH+YXacZ4IVqQvEOkHpI394Wjk2KL4DhJbDXRza60e6nKVSUTUIXjbXZ2WAex9NotBaLHo4JtGlI0ndp0zzixF/IhVuBqMphoawj8OmJNhdo/kK3oNIBLtyZPbkB7ALqXJ57MpbX8vZEVDApK2jIJQORlCUDFlLcmOS3JALcgKMoHIGLck1E5yS9JgW4RBAEYVSZKIIQpCQEqVClAEQsa1SpQB59/VnKqVTC/HLBrY9onnpeY+8Lw5+DNGvp6HfsV9K8Z4MVMDiAeTQ//wCCHfovEcfhvjVnf8begv8AouTLPXJT6aPRwYvyYrXaZf4fCBzG/ltI6jeEjOuI62GLaOGZNUiSQJDRyj+f2tqVKKLI6JeGwjQ7Vu7+cyuS6lydaVorsodmldhfVcQLxqmAIsbGd/JPx1OpoirUBIuC3UYP/Yk/VdJS1FsH2FgqzN6ADe61OdrocItOiy4KxztEEq2znLqdYRUph15E/SPcrmeHamhwHJd3Sio29u61i+Soxm+Mtihy/C0sOfCweoVwzFl8WgIK2Cg/sm0MOtRTToy9ZcsfVoCo3SecfcFVmT5RoptlulxfqcG9ATpB6q3d4Gud0aT7CU/KKjKjG1GmQQquClJEXl0i6NqlTaOV+acoUwumjzyFBRISgAShKIoSgBbktyY5KckMAoCjKW5AwHJL01yTUSAtwiCEIgqkwgpCEIkASpUBSkBilQpQAjG02vp1GPMMc1zT5OBB+68XxeWubUc5un4rXQejm7H3HNet55WcymHATDv3iV5rxTivhuDxTiQPmtP5ivP8uStL7PT8CWt+mTgHSx1P8tx5cwn4YS5c/wAP5yHYsMfpDagLABPzESL/AE9QuswVMB6i18UzrjJKTSLDD0JCqeJqDmsEbuMeQ6rpWPa1urkFymZ4r4rifZZm+BwdsRkdJwIB2mJPNd/hKA+HcxZef4DCO+I1zqjheblx+67TDZgIhoJ6k2H1VcDp8mPIV9G6+gIhpMjn/ZDQxMO0Ps7l0PkhpVAZLQ5x2OkTfoTyXM4fiZmMxXwadGow0qxYXVAAC+m6HhsEzaR6quRa/JEYpt6s7aJEctl5twpnFWgSxlQupSRDtw5pIJ7CQvSwYvyXh2GxBbVc74ha4lxDQIIl0+UGSp+Q2kmnyLBHe0z3nB4htRoc0gmBMdYWyub4UpBrGkEkuEuldIu3FLaKbPPyR1k0QhKJCVQmCUBRlAUDAclOTXJTkhiyluTHJbkhi3JNRPck1EgLcIghCIKxMlSFClICQiQqQgCVKhSkANWm1wLXCQdwVyfGeQMqYeRqJZceUG32XXLkOP3l2HqN+I5hYNbYJGojkSNweijnUdHZbA3uqPCsZV0VGub4XsOoahzBkH0IXqWCxjagp1mHwVGhw9RMeY29F5dxA74haWsdqc3U4uM7855K2/p5nzWxgazolxNEu2DiZNOe5uO5I5hcahtCj0Nqker4NwqM+GTfTHqJ+4I9lwGMxlalialLEubRpaiKLwDpcDManTYzE7brraNU03A9EGd4VmIaZEzdSi0u0Xi2nwwcnyFmJa2s3Eaqbntjb5C1pIPR1112AyTC0i4GoC0jm4W5HZeW4TIa7HzRJ3/CQD02cuky7JK50h7ngc9VQ7cwQCuiEoXwjWTHkkucqo6DiDOtLXYXAgfFe0sLmWNHYaxaJgmO8G6XlmQtof6fm9up7yd3PqHU4nvK3csy+lRvAnt/LqxZe6tJKSONyUFUf9fsquK8ybh8LUcZ8X+2NO8vtbuBJ9Fw+WcJtqg1mNeYII1PM6eh5FbPHWY68Y2i8H4VJgdb8Tn8+mwj3Vtw1mT8QzVQAawDS5p3lvfrC4cz2nTuv4KQuELRf5KK1NpJYBBM9wuiY6QCtfCUhobPQTK2V6GDG4Rqzzss9ndGISiQlWJAlAURQlAxbktyY5KckMApZRuQFIAHJT01yS9IZbhEEARBWJBKQhCIIGSpULEgCWKFKBErlP6gVKgoNa0wxxIfAlwEWN9h1K6taeY4UVGnU3UAHeHrI2UssXKDSKYpayTZ895zkFem+nT1guqTF7EbxNlyVXCxrGk6gbkm+69c4py/RhKrXvdUqw3QQ2RTpA+FpMeEjmd1xGV5fNSoaw1AcuZBG4I3IXnqWh6db8nQ8D53UxFB1PEOmpTcGhx3LSPDq6mxE9r9V1QcQFxfDeF+DinsAhlRhjzaZEj3XXNJbbl0OyxKSk7KRi0jZpVIMixVhTxDyLKtoMDjZXeFaGjxBGNm5UPwVJx3Ks3u0jSN1p06oGyKkbySuuLOXI7OK49wbqdRmLbJbAp1AOgJLT9SPZWn9PcHUdSfVDQGPPhvtyJV1mFNr2lj2gtIgg80XCobQLqI+Rzi5t7Nn8PuovGvzJsUsj/FR07BAAWLFi9E84woSpKEoAEoCjKAoGA5KcmOSnJDFuQFE5ASkALkp6YUp5SGWwRBAEYVyQQUhQFISAJYoUoAlSoWJASpULEAUXGL2NwtXWwuBabATJFwLLhMl4QcMPWxL5L3QaQ2hovcDuvWC2eUqix9JlOjqpv0umQwREz8uneFx58Sbcn6OvDlaWq9nnNPBFtem51QHEEzUDpkCOnkrZ4kKzGCfVrVS8MZTdEFwLj/ANeiq9V/Veeo0mejGWzGZePErt2yoWeF3mrMvOndbhIMkeTdY5MZUVcyryW0HwFWM7JOIzEPkEpLXGyNzrJJK1P2CRb4XOXNs8am9fxD91dYbFMqCWOntsR6Ljy5MpVCIIMHqLLUPIa4ZGfjp8rg7EoSqLC508eFzdfSLH9irSnjWkSQWnoYP1Fl2Qkpq0cc4ODpjigKIOBEgoCtmQHJbkxyS9IYtyWSjcllZYAkpNQppSaiTGi4CMJYKIFXJDAiCAIggAlKEKUgJUqAiAQBikLEitjabN3Se10hmwFFWk1wv79I5qhxefkAloAaASSbmFzGIzWtW8T3mDsBYAdIChmzLGuS2HDLI+C5zrHj4mii7U7ZzgLCOnUrl625PdbdF0BadR+686c9uT08UNeDYpDU2eYW5B0rUy8XjqtzEu0AqSKP0IwrpfHRWThZVOSHU97uUq7eAq4jGXhigUTGBIqPvZOouVHJE64F17bbLUfiYW1XeDYXK1KOCl417b6evQHso05S1iUTUY3Issrpkt+I7ntPT9yrA1rD9FqFx2BtzTA6ATA+y9fHHSKijyckt5bM22V9JjV/PNP/ANYeUHzVTrMxz78h+yfTqNtG/fb7q0Wn2Rkq6LRlcESRHn+6h6RRqGJNv50WxEi3JKUPtBGfNMS5LKJyAqLKglLemFLekxotQiCWCiBVyIwFEClgogUDGBGAltKY1IAgEGIrBjST6d0bnAAk7Bc/jcUXknlyHZIZFfFPqG5tyGwSAFEfRT/hCQ30aGaNlmn8xg+Qv+ipqdnFhHcLe4pxfwW0X8viaSO2kpTi14B9iF5nmfsPS8RVjAeIC1TSJMrag/m9wjp0A7clcx1LgDACHAKM6qWWydLNhYLRrE1DcWRZqK5s2MlaGME/Mblb9bEgLXotAEBHTw87pqTS4MySbtiWPc42C36WGMS4+ybSYByUYp1rLSh9sw580jTx+Po4dpJu7oE3JtbqYq1BDqg1x+Vn4B7X/wCyrWZe2rVDSJbMu8h+9h6ronntY9R06Lr8SFvY5fLmktV39iHGCTy7zt+qXUqG23ufZLfiIJIAjsR9+S1MI7U6wsJcZmB0A7bn0XacKN74gEgnxHf/ACn4dwIEkQbKvbVl5AiT1/X2W9TOk+cRstxEyxomOU+S36LtlX0XwSSRy2uR9FvUjtP1VSLQmu2CQklbmObYH0WjK5ZKmXi7RJQORISsmjda9GHLFi6CIWpEHLFiBDGlOYVixIZq5xV00j3MKjbcA9ZCxYsrs19AEmFNL5WkjdYsWvsX0c/x07/apDrUP0aVXYGqfhtHYLFi8ny/2Hr+J+s2m1CVs0HwsWLlOkl7pQTBClYgaN6iLBOEBYsVERZgrIMRVssWJNugrkPK2xT1nd5O28AwB9ym4iqXAkbi15iOyxYvXwJLGq9Hk523kf8AZVZm4ikY3g+wQ4SQ15m8x1+UQsWKi7MPozLnzqcd4MeYHO91ZYevtO+0xfqb8lixOI5FpRhsd+38lblJ9wsWKy6ISHYu9MnuP2VXqWLFz5OyuPoMFQ4rFimUP//Z"
          title="SHUKRIAAAAA"
        />
        <CardContent>
          <Typography component="p">
            ...
          </Typography>
        </CardContent>
        <CardActions className={classes.actions} disableActionSpacing>
          
          <IconButton
            className={classnames(classes.expand, {
              [classes.expandOpen]: this.state.expanded,
            })}
            onClick={this.handleExpandClick}
            aria-expanded={this.state.expanded}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
          <CardContent>
            
            <Typography paragraph>
             FAISFLKJNLVKAJDNVPIASNKASJN
            </Typography>
            <Typography paragraph>
              Heat oil in a (14- to 16-inch) paella pan or a large, deep skillet over medium-high
              heat. Add chicken, shrimp and chorizo, and cook, stirring occasionally until lightly
              browned, 6 to 8 minutes. Transfer shrimp to a large plate and set aside, leaving
              chicken and chorizo in the pan. Add pimentón, bay leaves, garlic, tomatoes, onion,
              salt and pepper, and cook, stirring often until thickened and fragrant, about 10
              minutes. Add saffron broth and remaining 4 1/2 cups chicken broth; bring to a boil.Heat oil in a (14- to 16-inch) paella pan or a large, deep skillet over medium-high
              heat. Add chicken, shrimp and chorizo, and cook, stirring occasionally until lightly
              browned, 6 to 8 minutes. Transfer shrimp to a large plate and set aside, leaving
              chicken and chorizo in the pan. Add pimentón, bay leaves, garlic, tomatoes, onion,
              salt and pepper, and cook, stirring often until thickened and fragrant, about 10
              minutes. Add saffron broth and remaining 4 1/2 cups chicken broth; bring to a boil.
            </Typography>
            
            
          </CardContent>
        </Collapse>
      </Card>
    );
  }
}

RecipeReviewCard55.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RecipeReviewCard55);