import axios from 'axios'
import auth_header from './auth_header'
const ROOT_URL='http://localhost:8000'
const blogServices = {
    getAllblogs,
    getBlog,
    getBlogComments,
    registerBlogComment,
    getAds

};

function getAllblogs() {
        return axios.get(ROOT_URL+'/api/blogs/')
}
function getBlog(id){
     return  axios.get(ROOT_URL+'/api/blogs/'+id)
}

function getBlogComments(id){
    return axios.get(ROOT_URL+'/api/getBlogComments/'+id)
}
function registerBlogComment(data){
    return axios.post(ROOT_URL+'/api/registerCommentBlog/',data)
}

function getAds(){
    return axios.get(ROOT_URL +'/api/ads/')
}

export default blogServices