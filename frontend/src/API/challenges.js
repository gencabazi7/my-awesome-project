import axios from 'axios'
import auth_header from './auth_header'
const ROOT_URL='http://localhost:8000'
const challengeServices = {
    getAllChallenges,
    getChallenge,
    getChallengeComments,
    registerChallengeComment
};

function getAllChallenges() {
        return axios.get(ROOT_URL+'/api/challenges/')
}
function getChallenge(id){
    return axios.get(ROOT_URL+'/api/challenges/'+id)
}
function getChallengeComments(id){
    return axios.get(ROOT_URL+'/api/getChallengeComments/'+id)
}
function registerChallengeComment(data){
    return axios.post(ROOT_URL+'/api/registerCommentChallenge/',data)
}
export default challengeServices;