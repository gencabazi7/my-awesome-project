import axios from 'axios'
import auth_header from './auth_header'
const ROOT_URL='http://localhost:8000'
const userServices = {
    signup,
    login,
    logout,
    // changePassword,
    // forgotPassword,
};

function login(username, password) {
    var data={
        "username": username,
        "password": password,
    }
        return axios.post(ROOT_URL+'/api/login/', data)
        .then(user =>{
            if(user){
                user.authdata= username;
                localStorage.setItem('user', JSON.stringify(user));

            }
            return user;
        
        }
        )
}


function signup(fullname,username,email,password,confirmPassword){
    var data={
        "first_name":'gzim',
        "last_name":'mehmeti',
        "username":username,
        "email":email,
        "password":password,
        "confirmPassword":confirmPassword

    }
    return axios.post(ROOT_URL+'/api/register/',data)

    
    
}
    function logout (){
        localStorage.removeItem('user');
    }
// function forgotPassword(email) {
//     var data={
//         "email":email,
//     }
//         return axios.post(ROOT_URL+'/api/forgotpassword/', data)
// }

export default userServices;