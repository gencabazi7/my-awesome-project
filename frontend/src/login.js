import React from 'react';
import './HomePage/Styling/css/login.css';
import MainHeader from './Header';
import {Link} from 'react-router-dom';
import Footer from './HomePage/Footer';
import userServices from './API/auth';
import LoginAlert from './Loginerror';


class Login extends React.Component {
  constructor(props){
    super(props)
    this.state={
      logindata:null,
      username:'',
      password:'',
      submitted: false,

    }
  }


  handleChange=(event)=>{
    const {name,value}= event.target;
    this.setState({[name]:value})
  }
  handleSubmit=(e)=> {
    e.preventDefault();

    this.setState({ submitted: true });
    const { username, password, returnUrl } = this.state;

    // stop here if form is invalid
    if (!(username && password)) {
        return;
    }

    this.setState({ loading: true });
    userServices.login(username, password)
        .then(
            user => {
                const { from } = this.props.location.state || { from: { pathname: "/" } };
                this.props.history.push(from);
            }).catch(error=> {
              if (error.response) {
                this.setState({error:<LoginAlert/>})
               
              }
            });;

}
    render() {
      let {username, password}=this.state
      return (
  <div>
      <MainHeader/>
        <div className='loginbody'>
          
        <br />
        <div className="row">
          <div className="border col-md-6 offset-md-3 col-lg-6 offset-lg-3">
            <h3 style={{marginTop: '10px'}} align="center">Login</h3>
            <h3 style={{marginTop: '10px'}} align="center">{this.state.error}</h3>
            <form name="form" onSubmit={this.handleSubmit}>
            <div className="container text-left">
              <div className='width:400px' >
                <label htmlFor="uname"><b>Username</b></label>
                <input style={{marginLeft: '70px'}} className='inputet' type="text" placeholder="Enter Username" name="username" value={username} onChange={this.handleChange} required />
                <hr className='hri'/>
                <label htmlFor="psw"><b>Password</b></label>
                <input style={{marginLeft: '70px'}} className='inputet' type="password" placeholder="Enter Password" name="password" value={password} onChange={this.handleChange} required />
                <br />
                <input  type="checkbox" defaultChecked="checked" name="remember" style={{float: 'center'}} /> Remember me 
                <br />
                <button className='butonet butonet-hover' type="submit">Login</button>
              </div>
              <div className="container" style={{backgroundColor: '#eeeeee'}}>
                <p style={{float: 'left'}}><a href="/signup">Register Now</a></p><a href="/signup">  
                </a><p align="right"><Link to='/forgotpassword'><a>Forgot Password?</a></Link></p>
                </div>
                </div>
                </form>
                </div>
                </div>
          </div>
        <Footer/>
          </div>
  
      );
    }
  }
  export default Login;