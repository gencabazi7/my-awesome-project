import React from 'react';
import ReactDOM from 'react-dom';
import Add from './HomePage/Styling/Images/add.png';
import News from './HomePage/Styling/Images/news.png';
import New from './HomePage/Styling/Images/New.png';
import MainHeader from './Header';
import Ads from './HomePage/Styling/Images/ads.png';
import Footer from './HomePage/Footer';
import './HomePage/Styling/css/allblogs.css';
import {Link} from 'react-router-dom';
import challengeServices from './API/challenges';
import Hidden from '@material-ui/core/Hidden';

class ChallengesMain extends React.Component {
  constructor(props){
    super(props)
    this.state={}

  }
  componentDidMount(){
    challengeServices.getAllChallenges().then(response=>{
      this.setState({challenges:response.data})
    })
    
  }
  spinner=()=>{
    return(  <div style={{marginTop:170}} className="text-center">
      <div class="spinner-grow text-muted"></div>
      <div class="spinner-grow text-primary"></div>
      <div class="spinner-grow text-success"></div>
      <div class="spinner-grow text-info"></div>
      <div class="spinner-grow text-warning"></div>
      <div class="spinner-grow text-danger"></div>
      <div class="spinner-grow text-secondary"></div>
      <div class="spinner-grow text-dark"></div>
      <div class="spinner-grow text-light"></div>
    </div>)

  }
  show_challenges=(items)=>{
   return items.map(item=>{
      return(
               <div key={item.id} className="col-lg-12" id="boxshadow" style={{borderRadius: '15px', marginBottom: '10px'}}>
                <center><h4><pre>{item.titulli} </pre></h4></center>
                <hr style={{borderBottom: '#fcf3cf  1.5px'}} />
                <p>{item.data} &amp; Time | Posted by ***</p>
                <div className="row">                <div className="col-lg-4">
                    <img src={item.foto} style={{height: '200px', maxWidth: '80%'}} />
                  </div>
                  <div className="col-lg-8" style={{paddingRight: '10px', float: 'right'}}>
                    <div style={{overflow: 'hidden', height: '150px'}}>
                      <p>{item.text}</p>
                    </div>
                    <a href={item.file} ><button  className="readMore btn btn-warning">Download</button> </a>
                    <Link to ={"/challenge/"+item.id}><button className="readMore btn btn-warning">Read More ...</button></Link>
                  </div>
                </div>
              </div>

      )
    })

  }
    render(){
      let challenges=this.state.challenges
        return(
          <div>
            <MainHeader/>
            <div>
       
        <h1>ALL Challenges</h1> 
        <hr style={{borderBottom: '#f1c40f   3px'}} />
        <div className="row ">
          <div style={{paddingLeft:"40px", paddingRight:"40px"}} className="col-lg-9">
            <div className="row">
            {challenges?this.show_challenges(challenges):this.spinner()}

            </div>
          </div>
          <Hidden only={['xs','sm','md']}>
          <div className="col-lg-3">
            <div id="boxshadow" style={{height: '800px', overflow: 'auto'}}>
              <img src="add.png" height={300} style={{marginBottom: '10px'}} />
              <img src="add.png" height={300} style={{marginBottom: '10px'}} />
              <img src="add.png" height={300} style={{marginBottom: '10px'}} />
              <img src="add.png" height={300} style={{marginBottom: '10px'}} />
              <img src="add.png" height={300} style={{marginBottom: '10px'}} />
              <img src="add.png" height={300} style={{marginBottom: '10px'}} />
              <img src="add.png" height={300} style={{marginBottom: '10px'}} />
              <img src="add.png" height={300} style={{marginBottom: '10px'}} />
              <img src="add.png" height={300} style={{marginBottom: '10px'}} />
            </div>
            
          </div>
          </Hidden>
        </div>
      </div>
      <Footer/>
      </div>
        )
    }
}

export default ChallengesMain;