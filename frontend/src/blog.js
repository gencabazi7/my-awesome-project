import React from 'react';
import Foto1 from './HomePage/Styling/Images/0_ngXgBNNdx6iiWP8q.png';
import Foto2 from './HomePage/Styling/Images/download.png';
import Foto3 from './HomePage/Styling/Images/52eabf633ca6414e60a7677b0b917d92-male-avatar-maker.jpg';
import Footer from './HomePage/Footer';
import './HomePage/Styling/css/blogstyle.css';
import MainHeader from './Header';
import blogServices from './API/blogs';



class Blog extends React.Component {


  constructor(props) {
    super(props)
    this.state = {Blog:[],Comments:[],username:JSON.parse(localStorage.getItem('user')).authdata,TextComment:""}

  }
onValueChange=(e)=>{
  const target= e.target;
  const value=target.value
  const name= target.name
  this.setState({
    [name]: value

  })

}
onSubmit=(e)=>{
  const { id } = this.props.match.params
  let {username,TextComment}=this.state
  e.preventDefault()
  let data= new FormData()
  data.append("username",username)
  data.append("blog_id",id)
  data.append("comment",TextComment)

  blogServices.registerBlogComment(data).then(response=>{
    if (response.data.succes==true){
      blogServices.getBlogComments(id).then(response =>{
        this.setState({Comments:response.data.comments?
        response.data.comments:[],TextComment:""})
      })

    }
    else {

    }

    
  })



}

  spinner = () => {
    return (<div style={{ marginTop: 170 }} className="text-center">
      <div class="spinner-grow text-muted"></div>
      <div class="spinner-grow text-primary"></div>
      <div class="spinner-grow text-success"></div>
      <div class="spinner-grow text-info"></div>
      <div class="spinner-grow text-warning"></div>
      <div class="spinner-grow text-danger"></div>
      <div class="spinner-grow text-secondary"></div>
      <div class="spinner-grow text-dark"></div>
      <div class="spinner-grow text-light"></div>
    </div>)

  }

  componentDidMount() {
    const { id } = this.props.match.params
    blogServices.getBlog(id).then(response => {
      this.setState({ Blog: response.data })
    })
    blogServices.getBlogComments(id).then(response =>{
      this.setState({Comments:response.data.comments?
      response.data.comments:[]})
    })
  }
  renderBlog = (blog) => {

    return (
      <div>
      <div className="col-lg-12 col-md-12 col-sm-12 post-title-block">
      <h2>{blog.titulli}</h2>
      <h5>{blog.data}</h5>
      <div className="image-block">
        <img src={blog.foto} width="70%" />
      </div>
      <div className="item">
        <div className="item-content-block">
          <div className="block-title">Tags</div>
        </div>
        <div className="item-content-block tags">
          <a href="#">lorem</a> <a href="#">loremipse</a> <a href="#">Esrite</a> <a href="#">remip</a> <a href="#">serte</a> <a href="#">quiaxms</a> <a href="#">loremipse</a> <a href="#">Esrite</a>
        </div>
      </div>
    </div>
    <div className="col-lg-9 col-md-9 col-sm-12">
      <p className="lead">{blog.permbajtja}</p>
  
   
    </div>
    </div>
    )
  }
  renderComments =(comments)=>{
    return comments.map(item=>{
      return(
        <div className="row">
    <div className="col-sm-2 text-center">
      <img src={Foto3} className="img-circle" height={65} width={65} alt="Avatar" />
    </div>
    <div className="col-sm-10">
      <h5>{item.user} <small>{item.data}</small></h5>
      <p>{item.text}.</p>
      <br />
    </div>
    </div>
    )
    })

    

  }
  render() {
    let thisblog=this.state.Blog
    let comments=this.state.Comments
    return (
      <div>
        <MainHeader colorr='#ff3b05' />
        <div>

          <section className="banner-section">
          </section>
          <section className="post-content-section">
            <div className="container">
              <div className="row">
                  {thisblog?this.renderBlog(thisblog):this.spinner()}
                <div className="col-lg-3  col-md-3 col-sm-12">
                  <div className="well">
                    <div className="input-group">
                    </div>
                  </div>
                  <div className="well">
                    <ul className="list-inline">
                      <li><span className="glyphicon glyphicon-heart" aria-hidden="true" /></li>
                      <li><span className="glyphicon glyphicon-heart" aria-hidden="true" /></li>
                      <li><span className="glyphicon glyphicon-heart" aria-hidden="true" /></li>
                      <li><span className="glyphicon glyphicon-heart" aria-hidden="true" /></li>
                    </ul>
                  </div>
                 
                
                </div>
              </div>
            </div> {/* /container */}
          </section>
          <h4>Leave a Comment:</h4>
          <form role="form" onSubmit={this.onSubmit} >
            <div className="form-group">
              <textarea value={this.state.TextComment} onChange={this.onValueChange} name="TextComment" className="form-control" rows={3} required defaultValue={""} />
            </div>
            <input type="submit" className="btn btn-success" value="Submit"/>

          </form>
          <br /><br />          
            {comments!=[]?this.renderComments(comments):''}
          
        </div>
        <Footer />
      </div>

    )
  };
};
export default Blog;