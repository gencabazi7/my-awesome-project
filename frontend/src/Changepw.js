import React from 'react';
import './HomePage/Styling/css/login.css';
import MainHeader from './Header';
import {Link} from 'react-router-dom';
import Footer from './HomePage/Footer';

class Changepw extends React.Component {
    render() {
      return (
  <div>
      <MainHeader/>
      <div>
        <br />
        <div className="row">
          <div className="borderchange col-md-6 offset-md-3 col-lg-6 offset-lg-3">
            <h4 style={{marginTop: '10px'}} align="center"><b>Change Your Password<b /></b></h4><b><b>
                <div className="containechange text-left">
                  <label htmlFor="psw">Old Password</label>
                  <input style={{marginLeft: '65px'}} className='inputchange'type="password" placeholder="Old Password" name="psw" required />
                  <hr className='hrchange'/>
                  <label htmlFor="psw"> New Password</label>
                  <input style={{marginLeft: '60px'}}  className='inputchange' type="password" placeholder="New Password" name="psw" required />
                  <hr className='hrchange' />
                  <label htmlFor="psw">Confirm Password</label>
                  <input style={{marginLeft: '30px'}}  className='inputchange' type="password" placeholder="Confirm Password" name="psw" required />
                  <hr className='hrchange' />
                  <button className='buttonchange' type="submit">Change</button>
                  <div className='width:400px'>
                  </div>
                </div>
              </b></b></div><b><b>
            </b></b></div></div>

        <Footer/>
          </div>
  
      );
    }
  }
  export default Changepw;