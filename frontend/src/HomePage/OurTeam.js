import React from 'react';
import Anetari1 from './Styling/Images/anetari1.png';
import Anetari2 from './Styling/Images/anetari2.png';
import Anetari3 from './Styling/Images/anetari3.png';
import Anetari4 from './Styling/Images/anetari4.png';
import Hidden from '@material-ui/core/Hidden';
import Grid from '@material-ui/core/Grid';

class OurTeam extends React.Component{
    render() {
      return (
        // <Grid item spacing={24}>
        //   <Grid item xs>
        <div className='row'>
        <div style={{maxWidth:'100%'}} className='col-4'>
          {/* Card Wider */}
      <div className="card card-cascade wider">
        {/* Card image */}
        <div className="view view-cascade overlay">
          <img className="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/photo6.jpg" alt="Card image cap" />
          <a href="#!">
            <div className="mask rgba-white-slight" />
          </a>
        </div>
        {/* Card content */}
        <div className="card-body card-body-cascade text-center pb-0">
          {/* Title */}
          <h4 className="card-title"><strong>Alison Belmont</strong></h4>
          {/* Subtitle */}
          <h5 className="blue-text pb-2"><strong>Graffiti Artist</strong></h5>
          {/* Text */}
          <p className="card-text">Sed ut perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
          {/* Linkedin */}
          <a className="px-2 fa-lg li-ic"><i className="fab fa-linkedin-in" /></a>
          {/* Twitter */}
          <a className="px-2 fa-lg tw-ic"><i className="fab fa-twitter" /></a>
          {/* Dribbble */}
          <a className="px-2 fa-lg fb-ic"><i className="fab fa-facebook-f" /></a>
          {/* Card footer */}
          <div className="card-footer text-muted text-center mt-4">
            2 days ago
          </div>
        </div>
      </div>
        </div>
        {/* // </Grid> */}
        {/* // <Grid item xs> */}
        <div style={{maxWidth:'100%'}} className='col-4'>          {/* Card Wider */}
      <div className="card card-cascade wider">
        {/* Card image */}
        <div className="view view-cascade overlay">
          <img className="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/photo6.jpg" alt="Card image cap" />
          <a href="#!">
            <div className="mask rgba-white-slight" />
          </a>
        </div>
        {/* Card content */}
        <div className="card-body card-body-cascade text-center pb-0">
          {/* Title */}
          <h4 className="card-title"><strong>Alison Belmont</strong></h4>
          {/* Subtitle */}
          <h5 className="blue-text pb-2"><strong>Graffiti Artist</strong></h5>
          {/* Text */}
          <p className="card-text">Sed ut perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
          {/* Linkedin */}
          <a className="px-2 fa-lg li-ic"><i className="fab fa-linkedin-in" /></a>
          {/* Twitter */}
          <a className="px-2 fa-lg tw-ic"><i className="fab fa-twitter" /></a>
          {/* Dribbble */}
          <a className="px-2 fa-lg fb-ic"><i className="fab fa-facebook-f" /></a>
          {/* Card footer */}
          <div className="card-footer text-muted text-center mt-4">
            2 days ago
          </div>
        </div>
      </div>
        </div>
        {/* // </Grid>
        // <Grid item xs> */}
        <div style={{maxWidth:'100%'}}className='col-4'>
          {/* Card Wider */}
      <div className="card card-cascade wider">
        {/* Card image */}
        <div className="view view-cascade overlay">
          <img className="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/photo6.jpg" alt="Card image cap" />
          <a href="#!">
            <div className="mask rgba-white-slight" />
          </a>
        </div>
        {/* Card content */}
        <div className="card-body card-body-cascade text-center pb-0">
          {/* Title */}
          <h4 className="card-title"><strong>Alison Belmont</strong></h4>
          {/* Subtitle */}
          <h5 className="blue-text pb-2"><strong>Graffiti Artist</strong></h5>
          {/* Text */}
          <p className="card-text">Sed ut perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque laudantium, totam rem aperiam. </p>
          {/* Linkedin */}
          <a className="px-2 fa-lg li-ic"><i className="fab fa-linkedin-in" /></a>
          {/* Twitter */}
          <a className="px-2 fa-lg tw-ic"><i className="fab fa-twitter" /></a>
          {/* Dribbble */}
          <a className="px-2 fa-lg fb-ic"><i className="fab fa-facebook-f" /></a>
          {/* Card footer */}
          <div className="card-footer text-muted text-center mt-4">
            2 days ago
          </div>
        </div>
      </div>
        </div>
        </div>
        // </Grid>
        // </Grid>
      );
    }
  };
export default OurTeam;