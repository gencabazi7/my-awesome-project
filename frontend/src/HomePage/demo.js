import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import Typography from '@material-ui/core/Typography';

import ButtonBase from '@material-ui/core/ButtonBase';
import SwipeableTextMobileStepper from  './stepper';
import OurTeam from './OurTeam';
import Divider from '@material-ui/core/Divider';

import MainHeader from '../Header';
import Footer from './Footer';
import Hidden from '@material-ui/core/Hidden';
import SwipeableTextMobileStepperm from '../mobilestepper';
import Image from './Styling/Images/0_ngXgBNNdx6iiWP8q.png';
import OurTeamm from './ourteammobile';
import blogServices from '../API/blogs';
import {Link} from 'react-router-dom';




const styles = theme => ({
  btn:{
  width: "100%",
  },
  root: {
    flexGrow: 1,
        justifyContent: 'center',
        width: '100%',
   
        backgroundColor: theme.palette.background.paper,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  button: {
    margin: theme.spacing.unit,
    justifyContent:'center',
  },
  input: {
    display: 'none',
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    minWidth: 300,
    width: '100%',
  },
  image: {
    position: 'relative',
    height: 200,
    [theme.breakpoints.down('xs')]: {
      width: '100% !important', // Overrides inline-style
      height: 100,
    },
    '&:hover, &$focusVisible': {
      zIndex: 1,
      '& $imageBackdrop': {
        opacity: 0.15,
      },
      '& $imageMarked': {
        opacity: 0,
      },
      '& $imageTitle': {
        border: '4px solid currentColor',
      },
    },
  },
  focusVisible: {},
  imageButton: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.common.white,
  },
  imageSrc: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: 'cover',
    backgroundPosition: 'center 40%',
  },
  imageBackdrop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: theme.palette.common.black,
    opacity: 0.4,
    transition: theme.transitions.create('opacity'),
  },
  imageTitle: {
    position: 'relative',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 4}px ${theme.spacing.unit + 6}px`,
  },
  imageMarked: {
    height: 3,
    width: 18,
    backgroundColor: theme.palette.common.white,
    position: 'absolute',
    bottom: -2,
    left: 'calc(50% - 9px)',
    transition: theme.transitions.create('opacity'),
  },


});

// const images = [
//   {
//     url: "https://pay.google.com/about/static/images/social/knowledge_graph_logo.png",
//     title: 'Google',
//     width: '40%',
//   },
//   {
//     url: "https://cdn0.vox-cdn.com/hermano/verge/product/image/8844/akrales_180913_2950_0066_squ.jpg",
//     title: 'i Phone',
//     width: '30%',
//   },
//   {
//     url: 'https://www.bestwebsitebuilders.uk/wp-content/uploads/2018/06/html-1080x500.png',
//     title: 'HTML5',
//     width: '30%',
//   },
// ];
class CenteredGrid extends React.Component {
  
  constructor(props){
    super(props)
    this.state={images:[]}
  }
  componentDidMount(){
    blogServices.getAllblogs().then(response=>{
      this.setState({images:response.data})
    })

  }
render(){

  let images = this.state.images.slice(0,6)
  const { classes } = this.props;
  

  return (
    <div>
      <MainHeader color='#ff3b05'/>
    <div className={classes.root}>
      <Grid container spacing={8}>
      <Hidden only={['xs','sm','md']}>
      <Grid item xs={1}>
  
  </Grid>
  </Hidden>
  <Hidden only={['xs','sm','md']}> <Grid item xs={3}>
         <br></br>
         <br></br>
         <br></br>
         <br></br>
          <center>
         
          <h1>CCSI</h1>
         <h3>Competence Center and
            Strategic Iinitiatives
          </h3>
          </center>
        </Grid>
        </Hidden>
        
        <Grid item xs={1}>
  
        </Grid>
       
        <Hidden only={['xs','sm','md']}>
        <Grid item xs={7} >
        <div  id="carouselExampleIndicators" className="carousel slide container-fluid" data-ride="carousel">
        <ol className="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to={0} className="active" />
          <li data-target="#carouselExampleIndicators" data-slide-to={1} />
          <li data-target="#carouselExampleIndicators" data-slide-to={2} />
          <li data-target="#carouselExampleIndicators" data-slide-to={3} />
          <li data-target="#carouselExampleIndicators" data-slide-to={4} />
        </ol>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img className="d-block w-100" src={Image} alt="First slide" />
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src={Image} alt="Second slide" />
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src={Image} alt="Third slide" />
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src={Image} alt="Fourth slide" />
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src={Image} alt="Fifth slide" />
          </div>
        </div>
        <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true" />
          <span className="sr-only">Previous</span>
        </a>
        <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true" />
          <span className="sr-only">Next</span>
        </a>
      </div>        </Grid>
        </Hidden>
        <Hidden only={['lg','xl']}>
        <Grid item xs={10} alignItems={'stretch'} position='center'>
        <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
        <ol className="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to={0} className="active" />
          <li data-target="#carouselExampleIndicators" data-slide-to={1} />
          <li data-target="#carouselExampleIndicators" data-slide-to={2} />
          <li data-target="#carouselExampleIndicators" data-slide-to={3} />
          <li data-target="#carouselExampleIndicators" data-slide-to={4} />
        </ol>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img className="d-block w-100" src={Image} alt="First slide" />
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src={Image} alt="Second slide" />
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src={Image} alt="Third slide" />
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src={Image} alt="Fourth slide" />
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src={Image} alt="Fifth slide" />
          </div>
        </div>
        <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true" />
          <span className="sr-only">Previous</span>
        </a>
        <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true" />
          <span className="sr-only">Next</span>
        </a>
      </div>
        </Grid>
        </Hidden>
        
        <Grid item xs={1}>
  
        </Grid>
       
       
        <Grid item xs={12}>
        <Divider/>
        </Grid>
       <Grid container spacing={8}>
        <Grid item xs={1}>

        </Grid>
        
          
        <Grid item xs={10}>
        <div className={classes.root}>
      {images.map(image => (
        

        <ButtonBase
          focusRipple
          key={image.titulli}
          className={classes.image}
          focusVisibleClassName={classes.focusVisible}
          style={{
            width: "33.33%",
          }}
        >
          <span
            className={classes.imageSrc}
            style={{
              backgroundImage: `url(${image.foto})`,
            }}
          />
          
          <Link to={"/blog/"+image.id}><span className={classes.imageBackdrop} />
          <span className={classes.imageButton}>
            <Typography
              component="span"
              variant="subtitle1"
              color="inherit"
              className={classes.imageTitle}
            >
              {image.titulli}
              <span className={classes.imageMarked} />
            </Typography>
          </span>
          </Link>
        </ButtonBase>
      ))}
    </div>
  

        </Grid>
     
        
        <Grid item xs={1}>

        </Grid>
        {/* <Hidden only={['lg','xl']}>
        <Grid item xs={1}>

      </Grid>
      </Hidden> */}
        </Grid>
       
        {/* <Grid item xs={2}>

</Grid> */}
<Grid container spacing={8}>
        <Grid item xs={1}>

        </Grid>
        
          
        <Grid item xs={10}>
        {/* <div className={classes.root}>
      {images.map(image => (
        <ButtonBase
          focusRipple
          key={image.title}
          className={classes.image}
          focusVisibleClassName={classes.focusVisible}
          style={{
            width: image.width,
          }}
        >
          <span
            className={classes.imageSrc}
            style={{
              backgroundImage: `url(${image.url})`,
            }}
          />
          <span className={classes.imageBackdrop} />
          <span className={classes.imageButton}>
            <Typography
              component="span"
              variant="subtitle1"
              color="inherit"
              className={classes.imageTitle}
            >
              {image.title}
              <span className={classes.imageMarked} />
            </Typography>
          </span>
        </ButtonBase>
      ))}
    </div> */}
  

        </Grid>
     
        
        <Grid item xs={1}>

        </Grid>
        {/* <Hidden only={['lg','xl']}>
        <Grid item xs={1}>

      </Grid>
      </Hidden> */}
        </Grid>

<Grid item xs={12}>
        <Divider/>
        </Grid>
        <Grid item xs={1}>
        </Grid>
        {/* <Grid item xs={1}>
        
        </Grid>
        <Grid className={'style'} item xs={1}>
   
        </Grid>  */}
        <Hidden only={['xs','sm','md']}>
        <Grid item xs={10}>
        <OurTeam/>
        </Grid>
        </Hidden>
        
        <Hidden only={['xl','lg']}>
        <Grid item xs={10}>
        <OurTeamm/>
        </Grid>
        </Hidden>
        <Grid item xs={1}>
        </Grid>
        </Grid>
        </div>
        <Footer/>
    </div>
  );
      }
}

CenteredGrid.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CenteredGrid);
