import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import CustomizedInputBase from './HomePage/SearchBar';
import SwipeableTemporaryDrawer from './HambMenu';
import {NavLink, Link} from 'react-router-dom';
import CSSILOGO from './HomePage/Styling/Images/123_8Tf_icon.ico';
import './HomePage/Styling/css/hoveri.css';
import Hidden from '@material-ui/core/Hidden';
import CustomizedInputBaseS from './HomePage/SearchBarS';


const styles = theme => ({
    btn:{
        width: "100%",
         backgroundColor:"#e65100",
          type:"button",
         fontStyle:"bold",
           color:"white",
          

          
        },
        navlbg:{
          backgroundColor:"#e65100",
        },

        root: {
          flexGrow: 1,
              justifyContent: 'center',
              width: '100%',
              
         
           
        }
        ,
        button:{
          backgroundColor:"#e65100",
          type:"button",
          fontStyle:"bold",
          color:"white",
          
        }
        ,
        logo:{
         height:"50px",
         width:'50px',
        }       ,
        grow: {
          flexGrow: 1,
        },
        appbarbgcolor: {
        // backgroundColor: '#ffa700',
        backgroundColor:'#1f2833',
       minwidth:'100%',

        },
      
       
        root: {
          display: 'flex',
          flexWrap: 'wrap',
          minWidth: 300,
          width: '100%',
          
        },
        textcolor: {
          textcolor : 'white',

        }
        
       
       
    });



function MainHeader(props) {
    const { classes } = props;
    
    
  
    return (
      <div>
      <div className={classes.root}>
        <Grid  container spacing={8}></Grid>
          <Grid item xs={12}>
          <div  className={classes.root}>
        <AppBar  className={classes.appbarbgcolor} >
          <Toolbar>
            <Grid item xs={1}>
          <SwipeableTemporaryDrawer/>
            {/* <Typography variant="h6" color="inherit" className={classes.grow}>
              CCSI Logo
            </Typography> */}
            </Grid>
            <Hidden only={['xs','sm']}>
            <Grid item xs={1}>
           <img className={classes.logo} src={CSSILOGO}/>
           </Grid>
           </Hidden>
           <Grid item xs={1}>
           </Grid>
           <Hidden only={['xs','sm','md']}>
           <Grid  item xs={1}>
            <CustomizedInputBase  style={'float-right'}/>
            </Grid>
            </Hidden>
            <Hidden only={['lg','xl']}>
           <Grid  item xs={1}>
            <CustomizedInputBaseS  style={'float-right'}/>
            </Grid>
            </Hidden>
            <Grid item xs={7}>
            </Grid>
            <Hidden only={['lg','xl','md']}>
            <Grid item xs={1}>
           <img className={classes.logo} src={CSSILOGO}/>
           </Grid>
           </Hidden>
            <Hidden only={['sm', 'xs',]}><Grid item xs={1}>
          <Link to='/login'><Button className={classes.button} >Login</Button>
           </Link>
          {/* <Hidden only={['lg','xl']}>
          <Grid item xs={5}>
          </Grid></Hidden> */}
           </Grid>
           </Hidden>
           <Hidden only={['sm', 'xs',]}>
           <Grid item xs={1}>
           <Link to='/signup'> <Button className={classes.button}>Sign up</Button>
           </Link>
           
           </Grid>
           </Hidden>
          </Toolbar>
        </AppBar>
      </div>
          </Grid>
          </div>
        <div style={{marginTop:8}}className={classes.root,'container-fluid'}>
      <AppBar style={{marginTop:70,backgroundColor:'transparent'}}>
        <Grid  container spacing={8}>
          <Grid item xs={3} >
          
          <NavLink className={classes.navlbg} activeStyle={{backgroundColor:"#1f2833"}} to="/home"><button  type="button" class="btn btn-primary btn-black btn-black-hover btn-block" style={{backgroundColor:"inherit"}}>Home</button>
        </NavLink>
          {/* <Link to="/"><Button variant="contained" size="large"  style={{backgroundColor:props.color}} className={classes.btn}>
          Home
        </Button>
        </Link> */}
        
        </Grid>

        

        <Grid item xs={3}>
        
        <NavLink className={classes.navlbg} activeStyle={{backgroundColor:"#1f2833"}} to="/blogs"><button  type="button" class="btn btn-primary btn-black btn-black-hover btn-block" style={{backgroundColor:"inherit"}}>Blogs</button>
        </NavLink>
        </Grid>
        <Grid item xs={3}>
        <NavLink className={classes.navlbg} activeStyle={{backgroundColor:"#1f2833"}} to ="/challenges"><button  type="button" class="btn btn-primary btn-black btn-black-hover btn-block" style={{backgroundColor:"inherit"}}>Challenge</button>
        </NavLink>
        </Grid>
        <Grid item xs={3}>
         
        <NavLink className={classes.navlbg} activeStyle={{backgroundColor:"#1f2833"}} to="/about"><button  type="button" class="btn btn-primary btn-black btn-black-hover btn-block" style={{backgroundColor:"inherit"}}>About</button>
        </NavLink>
          </Grid>
          <Grid item xs={12}>
          <Divider/>
          </Grid>
          </Grid>
          </AppBar>  
          </div>
          </div>
        
        
    )
    
}
MainHeader.propTypes = {
    classes: PropTypes.object.isRequired,
  }
  
  export default withStyles(styles)(MainHeader);
  