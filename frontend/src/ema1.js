import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


const styles = theme => ({
  card: {
    minWidth: 300,
    maxWidth: 300,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
});

class RecipeReviewCard66 extends React.Component {
  state = { expanded: false };

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  render() {
    const { classes } = this.props;

    return (
      <Card className={classes.card}>
        <CardHeader
          
          title="Ema Fisteku-Web Developer"
          
        />
        <CardMedia
          className={classes.media}
          image="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUQEhIVFRIVFRIVFxYVFRUVFRUVFRUWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uGB81ODMtNygtLisBCgoKDg0OGhAQGi0fHR0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSstLSstLS0tLSstLS0tLSstLS0tKy0tLf/AABEIAKwBJgMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAEBQIDBgABBwj/xABAEAABAwIEAggDBgYBAwUBAAABAAIDBBEFEiExQVEGEyJhcYGRobHB8AcjMlJy0RRCYoLh8bIzkqIWQ2OD0hX/xAAZAQADAQEBAAAAAAAAAAAAAAABAgMEAAX/xAAmEQACAgICAgEEAwEAAAAAAAAAAQIRAyESMUFRIjIzYXETI0IE/9oADAMBAAIRAxEAPwD7iuXLlxxy8dsvV4UH0cYDHcakYXNAIOuqz4rnHxW8x/A+saSN1hJKN0bsrhYrz5WjXFpg7oy43KJgpVbFEmuEU4MjQdrpKsc0PRTCerb1jtzstGoRCwCmvQxRUYmOTtnLly5UFOXLly445cuUXOsg3RxJcvAV6iccuXKLngalBujiS4oc1bUDXYhpZvFI8i8DKLFuNz5nG22wSgoqe5VAaplKKXDT62H+0JJcn2A8Pr3TCVmw4/M7JpQ4CLB0mx3bxPG1+XxXckjmhDQYe+Y6NuNhbQf93xPjudE8FBDFYPka23Bp1J4knf68UXWTZW5W9lo4A2v4m4WVxKsy3BuO8Bh/4OJ9VNtyHUfY+lxeBuma3efXil1ZipP/AE368rDQ766XWQlrCSe3dtiSLEECx1Id8jbRK6vFMji7UOvsBmcbna5NvbhzGhUUhrN1H0nDTaUjgP8AP1+138OIxubma64tsPLdfLcLY+V+d40BuLm/fcm2/oO7npmYl1bcrWtsOR1v5j2XODZyaH8uI8m6eI19CVGLEWOOU6H09lk5MRZIbXLXcwbHw038Pjsq/wCLLbNk1G4cNL/XxXcTrNw9o3B0UQs7huLZTYnMw699vzDn/vitBHrqDobajYg7WQTA0XtK5RauRFNauXLlrM5y5cuXHEJhoVlsbow5hNtQtTM6wKQYlJ2SOayZ2rLYjLRwplRNykO5KMcSKias5oNPRVQcO9FLMxkjY2V38U78x9VohmpUyMsPo0C5Iv4t/wCYqcdURuVVZr8E3jodLkHDWtO5RTXg7FUUkxGqJIPE5crb8iiy626R4zV37ISzfgKJxYqeIV4xPuSaMK9qmMNBXnkqZpy7dUMUiUHs4pkchZVdK5DPclboZEcq8Ea4uTHDoRrI7QDU9w5eJ+Hiu5aCTpaZkYMslgANz4bBYnpL9o4uWQWDRpmHaPq3bySX7R+l7ppDTQkCFpykk3zuGha1jdXa3Gug+CTCOi803bfcA8739OHuh0tjwhZKp6STyaiZwPIm4vy8fEKqLpHJ+CYXadMw4eNvlbyK0kPQ1g3Gquf0TYOHuUFL8FuBnJKo/jZqbANO5bzcLeFvNeUtHYZ3aW2G/nfa+yeHAspsNuWw9PrigsXaBYEaDlvtvyVIv2TlE9gro2jdrnc7X+Q+KofXNedyDz3t4jeyXSyxW/Bfhr2vgEOZxweBbhY2TCDWQaXNiODhqLdx3t56KcGIX+6fx2N/r69lTaxzTvoeW57xfj3bFV1UoNiLEHa2lj9aWSMdB1RXmJ1ieOh2tffy4/6W26I4t1rDGT2mXPlfW3dxHivmGIT54r31bob725H1+K0v2f4XWuLapkLjDYi92AuG12tJu4abgJafZzPp7SvUVSUGYZi4NB2G58+S5PxZOzRpdiGJNj0JVlRWgDRZrETmNyjlnekTjH2N4sbZ+ZXOxlnBYzYphTaqSlLwx+KG9RXOf3BBS3O6JhaoysUZp9spGkA2VsZUJGqLV0djNheZRL1XnUSUZaOTLhIvRKqGleldjkLIu6xXxVDhsSgrqbXLQ2TCZqpx0LihHOUJnqjrULOoPjKuDkDDKresQs6gzrFW+VDmVDyTI2dRdLKh3vVT5VW25SsKQbQxl7rcvr68ko+0rpMKWEQRavNjbm4ns389e/LZaaMtp4HSu0sCdd9ATqee/qvmOAYa/Eq59RLrFEb9xedm+Q181JS5SopGGrO6EdEC4ieYEuNjqvpUdE1osAjIKYMFgFGUqtex79Aj4wEO6O6JkKoc5cMgSohSLFsODxstI8oWdoIQsNHyPFaUxONu/h8xql5mB39rhbvpJQ3F7efI8F8/rY8pNuO4/ZPCV6JTjWy5rrbOzN4tcDb/AB4jzUetOwcMruZNwe89/wC3elgnIP7aXHOy8keDqPnv3D6/d6J2HF+4dqHNsQCDfwI3/wAnwX6F6NT3hYQABlFgBYAW0AHAL83U1NJUSNp4hd0ht3C/4nO7gLk+C/RmAQGOFkZNy1rRe1r2G9uCaKo67Gsklnn+oZvO9j8vVeKU1O19jmIIBGluNv2Xi6w6IyJdUtRzpEHUOWeUg8RFWmxR9A9D1MN1dSMsljIDiPIV7KFTC7RdLImk9AQPOFQCvZ5FQHqClspQRdcqM67OhOVhSCAFxVTZV6ZF0ZUKz0uUg5DF68EqtysSiydyBdIrZpUtqJrJ4qwBzJ0SyZImVStZUpqOHDp1RJKghMoumRSOCJJuG3MplgEHWuzZSImbXOhdwvzPE+SBocNkmIYBZosXu7+7meFvFalzmwxZW2AaD4eZ496jlmkhkm9Gf6cTvlb/AA8Iu5wO5AFuZJ4fXFWdFXUtJC2la+8g7T3EWzyO1c4d19AOQCjg9XGHS1Uh4ujjuODbdY+3IEht+eYIHpBBHWNL4ntEjdnNO/8AS4D6CGFKKt+Skl/ldI2D5UHNIsfgOLTRPFPUG+a+R3HTcLT9ZdO2NFHriq3KZKg4rhipxVEiueqnBIxhXXU2cEdy+Z9JaLI48l9Zn0Cw3S6mDmk8UVpivaPnbjfQ7/Wl1VI222t9LWub7Cw43/ZWS6eRstr9mnRozPFXI3sNJEQPEjQv8BqB5nktKMr7NP8AZv0T6hhmlH30m/8AQ3cMB9z387Bb9zsoUIY8oQ9XMum6Q0FbKv4lwJ1K5B5rrlnstQ5uoyBQzKWZLRxQ+NeNbZWOKjdGhWXscq5nqTVF7VzViC+V6hdXTNVCi4BsmHKLnqGZRkckkMi0Sr0SoNz1Fsq5HMLe9UvlsoBypqDotMeibRGaqS+plJXkpVLjdViBnkT0VHJcDu099/dCWV9INdN+7dLJnINEDzq0XA3I2Hjy80wwygD3AO7XEtbwF93Ha3mvKdlyA45CB5gAWvYatNidDZaTDiLWs6wI1ce0+w3N+V/2Szm0gpWGUrA1uRgDW67cSdxfcpJjTnTPbSxuAe654EMY22aRw4htxYHQuLQdCnT57AnYAcOfJL+jsFmy1BGsry1ptr1cZI1O+r+sPos0FznTK/SrFWP9GoHRtaM9mZQ1pkflIGpzAEXLjcl29yTxXz3G8Olgk62liljAA1a/OAf5szQ25G++i+s1eqXS0DXcFtUmlRzxrsz3Ryu/iY2PkblkBttbXgR3EcFqZOyNVThuDMY/MBb8JPLskkacDqvcVm0Km1RSOxfW4wyPe58EEzphBexzDyNkIXtJLn6AIaR9GTaSzf1AtSxk/R0kaeDFIpBdrxqr3His3T4HTEXjcQ0/lfcK9rpafT/qQ957bfA8R3FFtMCtB1Q9ZbpF+ErTl4cMwNwdkix+DMw+Cm2PVnzalww1NUynZftu7RHBg1c7xtfzIX33B6BkMbWNaA1rQABwAFgFgPsvwXtS1ThqT1TP0t1eR4usP7F9PYzRbIsySVFc8lknqn3KbTRpVUtUsjK40AySkaBcvJHhviuWey1GiLVGyseq7rRRm5EXNUQ1SJUC5dQORcxevVQcply6gMGmQMrrI6VLakpZI4j1qqkkQ8r1VnWWXYyYQ565qoDlfGVyQbL2hVytVjSucFoxisUzR6qEcV9/ja/gbFHyx6oeaJWQoLKCDYMHjcu89Db2V0FYPwgOtx7Qbf8AtaAFQWlX0zA43OhG4G58P8pW9hHNE4NZna0hzjZt+0bC13AeJttv4J7hQIGu+a2pNzoB631SRs1y1rRl0F7Ha52vbbKG6LQ4T+G4A0Nh3X1SZV8bDB7IYtP1cZtq85WtG4zOIaL89SmE3YaIwbhrQ2/Owtf5rN9IKjK+EcGzREjicrr/ALJlidVlcQTxKz/8702aXHaJuddWww3SD/8AoOc4NYLkm3qtSyPqm3Ju4/HiVrxuwZNFdYQ1tgsxiD81wmlfUXSZzrldkfgMI0CVGHZwAb2Gos5zTt3HVLK/BY+pdGyGPrCOy9w13BNyN7i4v3rVxN0Xr4AeCEZOPR0oKWmfH3UlXTPL2As43absPc5t/ithgWNmaMFw7VtRw77ft4LRz4Yw7hDswiJhu0ZT3JZy5eNghj4kacgCw25cksxyYBp8EynsNLrM9JJLMKmUo1H2dVANKwDnJ7yOW0aF8q+y2u7Ekd9Y5L/2ya/8g5fVIXXAWqGtGSe9ljo7hJMSjylPQUDi0F2E8tfIbpci0GDMdVO7S5ZmTpDEXkl9ybmzdbC/PbkuWTi34L2fUpXKkuXk0iHzLXRmouMgUOsVdlbFEijmqJtcrF6yNc82T0JyKJQl1U1MnlBTtQcQchNMFTdGzNVPVrNLHsZSK2Nur2RlFwQBFshVI4tAcwFmm6mEVLCFAR2TKFM7loq6pRkhRYRUFA9+zCe+xt6qjSQnIz8tOqoINeS1f/p6V38nm5wHtqUfS9FW27bje+uU8OQJCXi30juQhpIy7t22uLcwtDhtMcpNjxPwsEwp8GhZsD5uPqjA0NFmi3cFzxtrZymfP+kzLXdxsdPL69VPplA7rQ4E5TYm3e391PpdfK9xH8rQL/mdv7AK6qmE9NBKNc0bQf1N7LvcFYcaqLRvv5JkMEpGhge03vY3ROJVLw2+pte/hwWZg/ioqunihf8Adzl4e14uwZAXFw/Kco4b2CeYhO5pLX9g8D/K7w5eBWlfTo7i+QrZiHWmwRT4Wtbqks2FXOdoseBbp6EK0QzEZXPuPc+JSJvyUdDahqAQjgUii7DgfJM2zIp0dVhDygqiWy6WdK62pQbCC1tRqsn0kqriyc1ElySs5Xx57lJF7Oa0UdB8V6isGY2jl+6dyDifuyfO4/uX3fDZbtHovzdUM3ba52NuBHG/gvs/2dY0ainGY3kZZj+8gaO8x73Wr8mKvBuAvSL6KuMqwFM9o5HwnplgP8HVOY1v3TwXxn+knVvkdPRcvsPSbo5FWsayS4yOzBzfxC4sRfkdPQLxTCKH1SjFUhAZSvIxYp3JE+Q7ieCj49knp3I9kyEWgylYWXIWolXkkqXTucSnckThG2G51BypjBVwjK5SQZxoAnbqh0xlpyqxSpZNCWWUWqaxUt9rny0QNJFlO3rsmgq3m1t9tPQW71OWWjrPHYTI78Iv7fFMqPo6wayHMeQNm/uUypIcje0buO5OvkO5Vz1CtFUrl2ck5dE44Io/wsaPAC/qukrQErnqigpai+oKDylo4V5HbsTCkMQ8FnOuUf4g8TskeWQ/8MTQnE2i/IAkoRuMxuOQHtkZiONuF0hnqrXN7DS5PmqaCta6VxaNdsx4jTQeg9QnlKsV+WQUP7K8I86ZSdho72k+1vrvSr7PqzrIp6V28M2do/ok0I/72uP9yJ6Wy6HuI9rLP9AJMmJvZwkY9p9OsH/H3WLF3RulpWb2ogLMlQ1uZ0Jccu12uGV1u+xJV7hBWQ5m/wAzjcm2ZpBsQ6x0OmyvrQQyw39j3FZKogjL84JhlBJzDYG1iRbYkaXHBbYtVTFUHNWnTQXW9HSxxMEjmgNv2u1ryS6onliIbMy5Lc2ZmtgN7hD4gKsh4ZUv7YaLh7HDTS4u3M3TkUDiUtfOXjromNcGDss1blNzbU78b34bJZRj+iijlr5UwySvjdpmF+R0cOOoOoXsVcdj5HmlUPRVj39ZUSOldfNY2awH9Ld/NOmUrGi52AsFnn+DosHnrEE95JVjmZiqKiUNCnZRgNdLYIWCkc8XHkl+JVZJNvALOS4rNG85JHC3fcE8dDoqwxORGeVR7NDi9Fls42AO/K41B+I80y+zKv6up6v+WTs+ermfBw/uWGlxKWU/ePLhrpsPQJngtQ5kjXjcZXDxaQ4e4V1BxVMg8ik9H6MgddXJdh84exr27OAcPAi4TBiKAXMK5eMK8XUcY+SBUPjTqaBAzQ6rHPlejOyiFFMKrbErmRJU5IFkwLruoupxBFxsTJthUqBmQIhsSusouetEEwSnZW6JRbEufOAqhUp5LQll2RHYLDmlB4NBd57D4+yWGVOujP8A7jv0j4lZobyJHDWoellQ9F1Dkvmctcma4RpFEiXVsRHbbuNxzCPe5DyOU5KyqF0FSHC4U3v3Syv+6fnH4XHXkCVYakFvfsVG/BWiVRVWb+Em5AvppoeaEw6qFy69r6Nby31PfuhsQkPVaG1neoy/ul8M9jEw8XXPmbD2F/NapfbSMK+5JjTHJMwdfYEfXsUp6JOAxSF35sw88jh80Xij75u/L6jMPms1BiRinjmb+KKQG2mtiDlvyOgWGOpG57ifcKxgIskVXSX3AIRJxJkrWyMdmY8BzTzB28+7moGdbHTJRtCWXDWflCgKcN4JpMUDUzABTkqKcm+waQ+iAqZMx7gvKmsHNLKmuGwUJMdF9TUhosFnq+sLtArKiQuQ5isuSC2Kq2TI0uO6zltyd036QS7N70qaFtxqkYcruR0LE3w4doeBQUEfatyFz+3wR1ILEH62CMmCCPsX2d1nWUbATcxl0Z8Gm7P/AAcxa+Mr5b9ktd2qinPAh4HmWk+7fKy+oMSLscvC5RXJwC2UpfUyC66qq0pqq4XUJUhFGxkx6JjSWkq7lOaV11ObROSoKjjV4YpRhHQ06bEkLQrmdZATVK0c1FcbJFiGFWuRotCaA4sVzVC6GVBVTS06q2lchPo6Iya9aTow77uQ/wBQ+CyofZaPos+8cv6h8FlxfcGrYdUOQEhRU7kG8rQa0iDkNMr3FDyoMZCuvYHAtOx0WWFWY3GN2+3jyPw9VqqpZHpDEMwPMW8+ChkXktH0TrKlpi13z7crAfXkgHzfeXB0Btvyyj5oCR7y21za+vfovQ/Uk8XG3cOPyWhS+K/CMjj8n+xxiFR2QeZdfyIv81k6l9p3NPHXzH17p7Wydgf/AGelyFmsXP3+b9J+BWbGrkzS3UUNMK6STUpLW9qPMbscdPFp/lPt3LU4b07ilOQtcx/IgEeRH+F8+xFvaJGztfckIallyyB3eFdK1ZJunR9YqMXdyKT1WJPcrqGYPYFGamWdtlkLXSE7lcGozqApCNcGwTqUPU6BMJEmxaezSmihWzKYvJmk8FVTW/Edm6+Pd7KuY3JKstZniQPa/wBeK3LSoxN27CaBhs48T8dyj8th529wD80PSCzR6ny/17oic2j77H1It8X38lNvZRaQz+z2u6vEIv8A5OsjP94u3/ya0L7vHsvzLQVBjlZKPxRubIPFhuPgv0vSyBwBGxAI8DsufZy6L1y9C5McZmppLk6pRPhxutLMEK9qWcEycZUhPBRlvBOaMEKUbAimMAWfJjEk7L6Z2qeUySU+6c0yaCpHRDcqX4hELI66X4g82TtjRMbjEQugoNEZirruVULU0/pJS7LcuifdFD2ZR3s+aU20TLozo6X9LfiVlh9Y0e0MZyhyr5UO5aDYUyIaVEvCFmQYyFtWVkukklmg8itZWLDdKnnL5hSmUiLZZDkFjwP7XXshsBfcn4m5QzWdi9zw+N0bM3teDrBUk6iiEdzZKqOmXk0A+O5Hq4pHiAvK48tPMC314JtC67gT+b5n/wDISqLUkniXnzNv2UsWi+T0D15ORh4gZT66H4IGXe/mmFYLtH1uCgpNSO8NPrr81oh0QmtmwwSoNvrjr80761ZnCDqO9o+SfxlQa2WvRNzl5delQlOiFAsHqpLBZfFJS42TevkKUtYCSqwVbEk70Ip2WPmuy3aP1H4Afsr60a+aqp9h3OHuNfgtHgzNbGLRp6D3/wAL3EH2YOVzfw+j7KbRoPFvwv8AMqjEfwDx+QU12WfRRAztD6+v9L7/ANCqnrKOB3Hqw0+LOwf+K+DUGvl8rD5r7H9lryaO3BsjwPA2d8SUG/kFL4m1C5cFyoIf/9k="
          title="EMAAAAA"
        />
        <CardContent>
          <Typography component="p">
            ...
          </Typography>
        </CardContent>
        <CardActions className={classes.actions} disableActionSpacing>
          
          <IconButton
            className={classnames(classes.expand, {
              [classes.expandOpen]: this.state.expanded,
            })}
            onClick={this.handleExpandClick}
            aria-expanded={this.state.expanded}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
          <CardContent>
            
            <Typography paragraph>
             FAISFLKJNLVKAJDNVPIASNKASJN
            </Typography>
            <Typography paragraph>
              Heat oil in a (14- to 16-inch) paella pan or a large, deep skillet over medium-high
              heat. Add chicken, shrimp and chorizo, and cook, stirring occasionally until lightly
              browned, 6 to 8 minutes. Transfer shrimp to a large plate and set aside, leaving
              chicken and chorizo in the pan. Add pimentón, bay leaves, garlic, tomatoes, onion,
              salt and pepper, and cook, stirring often until thickened and fragrant, about 10
              minutes. Add saffron broth and remaining 4 1/2 cups chicken broth; bring to a boil.
            </Typography>
            
            
          </CardContent>
        </Collapse>
      </Card>
    );
  }
}

RecipeReviewCard66.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RecipeReviewCard66);