import React from 'react';
import './HomePage/Styling/css/Forgotpw.css';
import MainHeader from './Header';
import {Link} from 'react-router-dom';
import Footer from './HomePage/Footer';

class Forgotpw extends React.Component {
    render() {
      return (
  <div>
      <MainHeader/>
      <div>
      
        <br />
        <div className="row">
          <div className="borderforgot col-md-6 offset-md-3 col-lg-6 offset-lg-3">
            <h4 style={{marginTop: '10px'}} align="center"><b>Forgot Your Password?<b /></b></h4><b><b>
                <h6 style={{marginTop: '20px'}} align="center">Enter the email that you signed in with.</h6><h6>
                  <div className="container text-left">
                    <label htmlFor="uname"><b>Email</b></label>
                    <input style={{marginLeft: '95px'}} className='inputforgot'type="text" placeholder="Email" name="uname" required />
                    <hr className='hrforgot' />
                    <button className='buttonforgot'type="submit">Send</button>
                    <div className='width:400px'>
                    </div>
                  </div>
                </h6></b></b></div><b><b>
            </b></b></div>
          </div>
        <Footer/>
          </div>
  
      );
    }
  }
  export default Forgotpw;