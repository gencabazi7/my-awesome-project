import React , {Component} from 'react';
import ReactDOM from 'react-dom';

import Footer from './HomePage/Footer';

import OurTeam from './HomePage/OurTeam';
import CenteredGrid from './HomePage/demo';
import AllBlogs from './AllBlogs';
import MainHeader from './Header';
import {BrowserRouter , Route, Switch } from 'react-router-dom';
import Error from './Error';
import CenteredGridd from './demo1';
import ChallengesMain from './challenges';
import Login from './login';
import Signup from './signup';
import Forgotpw from './Forgotpw';
import Challengegrid from './challengegrid';
import Blog from './blog';
import ALLBLOG from './ALLBLOGF';
import challenge from './challenge';
    




class App extends React.Component{
    render(){
    return (
       
    
        <BrowserRouter >
        <div>
            
            <Switch>
            <Route path={"/about"} component={CenteredGridd}/>
            <Route path={"/blogs"} component={ALLBLOG}/>
            <Route path={"/home"} component={CenteredGrid} exact />
            <Route path={"/challenges"} component={ChallengesMain}/>
            <Route path={"/login"} component={Login}/>
            <Route path={"/signup"} component={Signup}/>
            <Route path={"/forgotpassword"} component={Forgotpw}/>
            <Route path={"/challenge/:id"} render={props=>(<Challengegrid{...props}/>)}/>
            <Route path={"/blog/:id"} component={Blog}/>
            <Route path={"/"} component={CenteredGrid} exact />
            <Route component={Error}/>
            </Switch>
       
        </div>
        </BrowserRouter>
      

      
    )
    
}



};
ReactDOM.render(<App />, document.getElementById('root'));
