import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';

import CardMedia from '@material-ui/core/CardMedia';


const styles = {
  card: {
    maxWidth:1000 ,
  },
  
  media: {
    // ⚠️ object-fit is not supported by IE 11.
    objectFit: 'cover',
  },
};

function ImgMediaCardd(props) {
  const { classes } = props;
  return (
    <div className={"container-fluid"}>
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt="ASTRONAUT'S EYE"
          className={classes.media}
          height="300"
          image="https://www.futurity.org/wp/wp-content/uploads/2019/01/astronaut-selfie-in-space_1600.jpg"
          title="ASTRONAUT'S EYE"
        />
 
      </CardActionArea>
     
    </Card>
    </div>
  );
}

ImgMediaCardd.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ImgMediaCardd);
