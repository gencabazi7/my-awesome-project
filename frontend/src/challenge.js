import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import challengeServices from './API/challenges';


const styles = {
  card: {
    maxWidth:1000 ,
  },
  media: {
    // ⚠️ object-fit is not supported by IE 11.
    objectFit: 'cover',
  },
};


class ImgMediaCard extends React.Component{
  constructor(props){
    super(props)

  }

  render(){
  const { classes } = this.props;
  const {challenge}=this.props
  
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt="ASTRONAUT'S EYE"
          className={classes.media}
          height="300"
          image={challenge.foto}
          title="ASTRONAUT'S EYE"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" >
          {challenge.titulli}
          </Typography>
          <Typography variant="h6" gutterBottom component="p">
  
          {challenge.text}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>


      </CardActions>
    </Card>
  );
}
}

ImgMediaCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ImgMediaCard);
