import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Hidden from '@material-ui/core/Hidden';
import Grid from "@material-ui/core/Grid";
import RecipeReviewCard1 from './filanfisteku1';

import RecipeReviewCard from './filanfisteku';

import MainHeader from './Header';
import Footer from './HomePage/Footer';
import Divider from '@material-ui/core/Divider';


const styles = theme => ({
  root: {
    flexGrow: 1,
    padding:20
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: "center",
    color: theme.palette.text.secondary
  }
});

function CenteredGridd(props) {
  const { classes } = props;

  return (
    <div>
      <MainHeader/>
    <div className="container-fluid" >
      <div className='row' >
        <div style={{marginBottom: 15}} className='col-lg-4 col-md-4 col-sm-6 '>
        <RecipeReviewCard />
        </div>
        <div style={{marginBottom: 15}} className='col-lg-4 col-md-4 col-sm-6'>
        <RecipeReviewCard />
        </div>
        <div style={{marginBottom: 15}} className='col-lg-4 col-md-4 col-sm-6'>
        <RecipeReviewCard />
        </div>
        <div  style={{marginBottom: 15}} className='col-lg-4 col-md-4 col-sm-6'>
        <RecipeReviewCard />
        </div>
        <div  style={{marginBottom: 15}} className='col-lg-4 col-md-4 col-sm-6'>
        <RecipeReviewCard />
        </div>
        <div   style={{marginBottom: 15}}className='col-lg-4 col-md-4 col-sm-6'>
        <RecipeReviewCard />
        </div>
      </div>
      
      </div>
      <Footer/>
    </div>
    // <div >
    // <MainHeader colorrrr='#ff3b05'/>
    // <div className={classes.root + "container-fluid"}  >
     
    //   <div className="container-fluid row "style={{marginBottom:"15px"}}>
    //   {/* <Grid container spacing={12}>
    
       
    //       <Grid item xs={4}> */}
    //     <div  style={{marginBottom:"15px"}} className='col-4'>
    //       <Hidden only={['xs','sm','md']}>
    //         <RecipeReviewCard />
    //         </Hidden>
    //         <Hidden only={['xl','lg']}>
    //         <RecipeReviewCard1 />
    //         </Hidden>
    //     </div>
    //     {/* </Grid>
    //     <Grid item xs={4}> */}
    //     <div style={{marginBottom:"15px"}} className='col-4'> 
    //     <Hidden only={['xs','sm','md']}>         
    //       <RecipeReviewCard/>
    //       </Hidden>
    //       <Hidden only={['xl','lg']}>
    //         <RecipeReviewCard1 />
    //         </Hidden>
    //     </div>
    //     {/* </Grid>
    //     <Grid item xs={4}> */}
    //     <div style={{marginBottom:"15px"}} className='col-4'>
    //     <Hidden only={['xs','sm','md']}>
    //       <RecipeReviewCard/>
    //       </Hidden>
    //        <Hidden only={['xl','lg']}>
    //         <RecipeReviewCard1 />
    //         </Hidden>
    //     </div>
    //     {/* </Grid>
    //     </Grid> */}
    //     </div>
        
    //     <div className="container-fluid"style={{marginBottom:"15px"}}>
      
    
    
    //     {/* <Grid container spacing={12}>
    //       <Grid item xs={4}> */}
    //     <div style={{marginBottom:"15px"}}>
    //       <Hidden only={['xs','sm','md']}>
    //         <RecipeReviewCard />
    //         </Hidden>
    //         <Hidden only={['xl','lg']}>
    //         <RecipeReviewCard1 />
    //         </Hidden>
    //     </div>
    //     {/* </Grid> */}
    //     {/* <Grid item xs={4}> */}
    //     <div style={{marginBottom:"15px"}}> 
    //     <Hidden only={['xs','sm','md']}>         
    //       <RecipeReviewCard/>
    //       </Hidden>
    //       <Hidden only={['xl','lg']}>
    //         <RecipeReviewCard1 />
    //         </Hidden>
    //     </div>
    //     {/* </Grid> */}
    //     {/* <Grid item xs={4}> */}
    //     <div style={{marginBottom:"15px"}} >
    //     <Hidden only={['xs','sm','md']}>
    //       <RecipeReviewCard/>
    //       </Hidden>
    //        <Hidden only={['xl','lg']}>
    //         <RecipeReviewCard1 />
    //         </Hidden>
    //     </div>
    //     {/* </Grid>
    //     </Grid> */}
        
    //     </div>
    // </div>
    // <Footer/>
    // </div>
  );
}

CenteredGridd.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CenteredGridd);
