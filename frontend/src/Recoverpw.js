import React from 'react';
import './HomePage/Styling/css/Recoverpw.css';
import MainHeader from './Header';
import {Link} from 'react-router-dom';
import Footer from './HomePage/Footer';

class Recoverpw extends React.Component {
    render() {
      return (
  <div>
      <MainHeader/>
      <div>
        <br />
        <div className="row">
          <div className="borderrecover col-md-6 offset-md-3 col-lg-6 offset-lg-3">
            <h4 style={{marginTop: '10px'}} align="center"><b>Recover Your Password<b /></b></h4><b><b>
                <div className="containerrecover text-left">
                  <label htmlFor="psw"> New Password</label>
                  <input style={{marginLeft: '60px'}} className='inputrecover'type="password" placeholder="New Password" name="psw" required />
                  <hr  className='hrrecover' />
                  <label htmlFor="psw">Confirm Password</label>
                  <input style={{marginLeft: '30px'}} className='inputrecover' type="password" placeholder="Confirm Password" name="psw" required />
                  <hr className='hrrecover' />
                  <button className='buttonrecover'type="submit">Recover</button>
                  <div className='width:400px'>
                  </div>
                </div>
              </b></b></div><b><b>
            </b></b></div></div>

        <Footer/>
          </div>
  
      );
    }
  }
  export default Recoverpw;