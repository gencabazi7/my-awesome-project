import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

function ContainedButtons(props) {
  const { classes } = props;
  return (
    <div>
      
      <Button variant="contained" color="primary" className={classes.button}>
        Home
      </Button>
      <Button variant="contained" color="primary" className={classes.button}>
        About Us
      </Button>
      <Button variant="contained" color="primary" className={classes.button}>
        Community
      </Button>
      <Button variant="contained" color="primary" className={classes.button}>
        Blog
      </Button>
     
      
    </div>
    
  );
}

ContainedButtons.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ContainedButtons);