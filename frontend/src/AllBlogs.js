import React from 'react';
import ReactDOM from 'react-dom';
import Add from './HomePage/Styling/Images/add.png';
import News from './HomePage/Styling/Images/news.png';
import New from './HomePage/Styling/Images/New.png';
import MainHeader from './Header';
import Ads from './HomePage/Styling/Images/ads.png';
import Footer from './HomePage/Footer';
import './HomePage/Styling/css/allblogs.css';
import {Link} from 'react-router-dom';

class AllBlogs extends React.Component {
  render() {
    return (

      <div  >
        <MainHeader colorr='#ff3b05'/>
        <div className = 'container-fluid'>
        <title>Blog</title>
        {/* Latest compiled and minified CSS */}
        {/* jQuery library */}
        {/* Latest compiled JavaScript */}
        <h1>ALL BLOGS</h1>
        <hr style={{ borderBottom: 'solid black 3px' }} />
        <div className='row '>

          <div className="col-lg-8">
            <div className="col-lg-12">
              <h2>Lajmi 1</h2>
              <hr style={{ borderBottom: 'solid black 2px' }} />
              <p>Date &amp; Time | Posted by ***</p>
              <table>
                <tbody><tr>
                  <td rowSpan={2}>
                    <img src={New} height={200} />
                  </td>
                  <td><div  className='blogcontent' >
                    Description</div>

                  </td>
                </tr>
                  <tr>
                    <td>
                      <Link to ="/blog1"><button className="readMore btn btn-warning">Read More ...</button></Link>
                    </td>
                  </tr>
                </tbody></table>
            </div>
            <div className="col-lg-12">
              <h2>Lajmi 2</h2>
              <hr style={{ borderBottom: 'solid black 2px' }} />
              <p>Date &amp; Time | Posted by ***</p>
              <table>
                <tbody><tr>
                  <td rowSpan={2} >
                    <img src={New} height={200} />
                  </td>
                  <td >
                  <div className='blogcontent'>
                  Naimi lindi në fshatin Frashër të Vilajetit të Janinës (sot në rrethin e Përmetit), i biri i Halit beut (1797–1859) dhe Emine hanëmit (1814–1861). Nga i ati ishin pasardhës fisnikësh e timarlinjsh me prejardhje nga Berati që më vonë u njohën si Dulellarët, ndërsa familja e së ëmës qenë pinjoj të Iljaz bej Mirahorit.[3]

Në vendlindje bëri mësimet fillore dhe nisi të mësonte turqishten osmane, arabishten dhe persishten në Teqenë e Frashërit,[4] si myhib bektashi.[5] Pas vdekjes së prindërve, me në krye vëllain e madh që ishte bërë zot shtëpie, Abdylin, më 1865 familja u shpërngul në Janinë, ku bashkë me vëllanë më të vogël Samiun, mbaroi gjimnazin grek "Zosimea" më 1869.[4] Në "Zosimea" mori një kulturë të gjërë për kohën, u njoh me kulturat klasike, mësoi gjuhën greke dhe gjuhën frënge. Atje ra në kontakt me idetë e iluminizmit frëng dhe veprat e Rusoit e Volterit.[6]

Më 1871 shkoi në Stamboll, ku qëndroi vetëm tetë muaj, sepse dhembja e kraharorit e detyroi të kthehej në Janinë. Në vitet 1874-76 punoi si drejtor dogane në Sarandë. Sëmundja e kraharorit dhe dhembjet në gjunjtë nga reumatizma, e shtrënguan të largohej nga Saranda dhe shkoi për kurim gjashtë muaj në banjat e Badenit në Perandorinë Austro-Hungareze. Më 1878 punoi tetë muaj si drejtor të dhjetash (ashari mydiri) në Berat. Më 1882 u vendos përfundimisht në Stamboll,[7] ku punoi në fillim në detyrën e anëtarit e pastaj të Kryetarit të Komisionit të Inspektimit dhe Kontrollit dhe më vonë, të Kryetarit të Këshillit të Lartë të Arsimit dhe të Zëvendësministrit të Arsimit. Më 14 gusht 1882, nënshkroi lejen për botimin e Gramatikës së Kristoforidhit.[4] Pas arrestimit të Abdylit në Janinë në fund të prillit 1881, nisi të luajë një rol të rëndësishëm në veprimtarinë kombëtare të shqiptarëve të atjeshëm. Mori pjesë në punën e Komitetit qendror për mbrojtjen e të drejtave të kombësisë shqiptare dhe të Shoqërisë së të shtypuri shkronja shqip.[8]

Në vitin 1884 u shfaq revista shqiptare Drita me redaktor Petro Poga dhe më vonë Pandeli Sotiri, ndërsa Naim Frashëri ishte redaktor i fshehtë pasi që në atë kohë shqiptarët muslimanë nuk lejoheshin të shkruanin shqip nga autoritetet osmane.[9] Autoritetet osmane ndalonin shkrimin e shqipes, si rrjedhojë veprat publikoheshin jashtë shtetit dhe Frashëri përdorte inicialet N.H.F. [10] Në një letër të vitit 1887, Frashëri shprehte mendimet e tij mbi gjendjen e Perandorisë Osmane në atë kohë. Sipas tij zgjidhja më e mirë për të ardhmen e shqiptarëve ishte bashkimii i Shqipërisë me Austro-Hungarinë.[11]

Mbylli sytë më 20 tetor 1900
</div>
                  </td>
                </tr>
                  <tr>
                    <td>
                      <Link to="/blog1"><button className="readMore btn btn-warning">Read More ...</button></Link>
                    </td>
                  </tr>
                </tbody></table>
            </div>
          </div>
          <div className="col-lg-4 text-center" >
              <img src={Add} height={300} />
          </div>
        

        </div>
        <button className="float-left btn btn-secondary" style={{ backgroundColor: '#515145' }}>Previous Page</button>
        <button className="float-right btn btn-secondary" style={{ backgroundColor: '#515145' }}>Next Page</button>
        </div>
        <Footer />
      </div>

    );
  }
}

export default AllBlogs;