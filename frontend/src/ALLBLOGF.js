import React from 'react';
import './HomePage/Styling/css/style.css';
import MainHeader from './Header';
import Footer from './HomePage/Footer';
import Hidden from '@material-ui/core/Hidden';
import './HomePage/Styling/css/allblogs.css';
import {Link} from 'react-router-dom';
import blogServices from './API/blogs';
import Example from './pagination';




class ALLBLOG extends React.Component{
  constructor(props){
    super(props)
    this.state={}

  }
  
  componentDidMount(){
    blogServices.getAllblogs().then(response=>{
      this.setState({blogs:response.data})
    })
    blogServices.getAds().then(response=>{
      this.setState({ads:response.data})
      
    })
    
  }
  spinner=()=>{
    return(  <div style={{marginTop:170}} className="text-center">
      <div class="spinner-grow text-muted"></div>
      <div class="spinner-grow text-primary"></div>
      <div class="spinner-grow text-success"></div>
      <div class="spinner-grow text-info"></div>
      <div class="spinner-grow text-warning"></div>
      <div class="spinner-grow text-danger"></div>
      <div class="spinner-grow text-secondary"></div>
      <div class="spinner-grow text-dark"></div>
      <div class="spinner-grow text-light"></div>
    </div>)

  }
  show_blogs=(items)=>{
   return items.map(item=>{
      return(
               <div key={item.id} className="col-lg-12" id="boxshadow" style={{borderRadius: '15px', marginBottom: '10px'}}>
                <center><h4><pre>{item.titulli} </pre></h4></center>
                <hr style={{borderBottom: '#fcf3cf  1.5px'}} />
                <p>{item.data} &amp; Time | Posted by ***</p>
                <div className="row">                <div className="col-lg-4">
                    <img src={item.foto} style={{height: '200px', maxWidth: '80%'}} />
                  </div>
                  <div className="col-lg-8" style={{paddingRight: '10px', float: 'right'}}>
                    <div style={{overflow: 'hidden', height: '150px'}}>
                      <p>{item.permbajtja}</p>
                    </div>
                    <Link to ={"/blog/"+item.id}><button className="readMore btn btn-warning">Read More ...</button></Link>
                  </div>
                </div>
              </div>

      )
    })

  }
  show_ads=(items)=>{
    return items.map(item=>{
    return(
      
      <Link to = {item.link}>
        <img src={item.foto} height={300} style={{marginBottom: '10px'}} />
        </Link>
    

      
    )
  })
}


    render(){
      let blogs=this.state.blogs
      let ads=this.state.ads
        return(
          <div>
            <MainHeader/>
            <div>
       
        <h1>ALL BLOGS</h1> 
        <hr style={{borderBottom: '#f1c40f   3px'}} />
        <div className="row ">
          <div style={{paddingLeft:"40px", paddingRight:"40px"}} className="col-lg-9">
            <div className="row ">
            {blogs?this.show_blogs(blogs):this.spinner()}

            </div>
            <div className="text-center">
            <Example></Example>
            </div>
          </div>
          <Hidden only={['xs','sm','md']}>
       
          <div className="col-lg-3">
          <div id="boxshadow" style={{height: '165vh', overflow: 'auto'}}>
          {ads?this.show_ads(ads):this.spinner()}
          </div>
            
          </div>
          </Hidden>
        </div>
 
      </div>
      <Footer/>
      </div>
        )
   
}}

export default ALLBLOG;